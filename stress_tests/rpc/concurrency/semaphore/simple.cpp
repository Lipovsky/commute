#include <commute/rpc/concurrency/semaphore/simple.hpp>

#include <wheels/test/test_framework.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/futures/make/execute.hpp>
#include <await/futures/terminate/go.hpp>

using commute::rpc::concurrency::Semaphore;

TEST_SUITE(SimpleSemaphore) {
  SIMPLE_TEST(Stress) {
    static const size_t kPermits = 3;
    static const size_t kThreads = 8;
    static const size_t kItersPerThread = 1234567;

    Semaphore sema(kPermits);

    std::atomic<size_t> access{0};
    std::atomic<size_t> load{0};

    await::executors::ThreadPool pool{kThreads};

    for (size_t i = 0; i < kThreads; ++i) {
      await::futures::Execute(pool, [&]() {
        for (size_t j = 0; j < kItersPerThread; ++j) {

          twist::ed::SpinWait spin_wait;
          while (!sema.TryAcquire()) {
            spin_wait();
          }

          ASSERT_TRUE(load.fetch_add(1) < kPermits);

          ++access;

          load.fetch_sub(1);

          sema.Release();
        }
      }) | await::futures::Go();
    }

    pool.WaitIdle();

    std::cout << "Counter = " << access
              << " (expected = " << kThreads * kItersPerThread << ")"
              << std::endl;

    ASSERT_EQ(access, kThreads * kItersPerThread);

    pool.Stop();
  }
}
