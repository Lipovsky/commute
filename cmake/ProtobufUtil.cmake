function(ProtobufCompile name)
    # Find `protoc` binary
    FetchContent_GetProperties(protobuf BINARY_DIR protobuf_BINARY_DIR)
    set(ProtobufCompiler ${protobuf_BINARY_DIR}/protoc)

    # Proto directory
    set(ProtoSrcDir ${CMAKE_CURRENT_LIST_DIR})
    ProjectLog("Proto dir: ${ProtoSrcDir}")

    if (NOT EXISTS ${ProtobufCompiler})
        ProjectWarning("Protobuf compiler not found, skipping ${ProtoSrcDir}")
        return()
    endif()

    file(GLOB ProtoFiles RELATIVE ${ProtoSrcDir} "*.proto")
    ProjectLog("Proto files: ${ProtoFiles}")

    # Output directory
    set(ProtoCppOutDir ${ProtoSrcDir}/cpp)

    ProjectLog("Proto out cpp dir: ${ProtoCppOutDir}")

    if (NOT EXISTS ${ProtoCppOutDir})
        file(REMOVE_RECURSE ${ProtoCppOutDir})
        file(MAKE_DIRECTORY ${ProtoCppOutDir})

        # Compile
        execute_process(
            COMMAND ${ProtobufCompiler} --proto_path=${ProtoSrcDir} --cpp_out=${ProtoCppOutDir} ${ProtoFiles}
        )
    endif()

    # List compiled headers/sources
    file(GLOB_RECURSE ProtoCppSources ${ProtoCppOutdir} "*.h" "*.cc")
    ProjectLog("Compiled protos: ${ProtoCppSources}")

    # Add library
    set(ProtosLib ${name}_protos)
    add_library(${ProtosLib} ${ProtoCppSources})
    target_link_libraries(${ProtosLib} libprotobuf)

    # Mark `<google/protobuf/...>` includes as system ones so that
    # compiler won't generate warnings about `protobuf` code.
    FetchContent_GetProperties(protobuf SOURCE_DIR protobuf_SOURCE_DIR)
    target_include_directories(
      ${ProtosLib}
      SYSTEM
      PUBLIC "${protobuf_SOURCE_DIR}/src")

    ProjectLog("Protos lib: ${ProtosLib}")

    # Add manual `compile` target
    add_custom_target(${name}_compile_protos
            COMMAND rm -rf ${ProtoCppOutDir}
            COMMAND mkdir ${ProtoCppOutDir}
            COMMAND ${ProtobufCompiler} --proto_path=${ProtoSrcDir} --cpp_out=${ProtoCppOutDir} ${ProtoFiles}
            COMMENT "Compile protobuf messages for ${name} (${ProtoSrcDir})"
            VERBATIM
    )

endfunction()
