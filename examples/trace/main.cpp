#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/filters/furnish.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/observe/trace.hpp>

#include <commute/rpc/filters/route.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sched/self.hpp>
#include <await/await.hpp>

#include <timber/trace/scope.hpp>
#include <timber/log/log.hpp>

#include "proto/cpp/greeting.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class GreetingService final :
    public rpc::ServiceBase<GreetingService> {
 public:
  static constexpr std::string_view kName = "Greeting";

  GreetingService()
      : logger_(kName) {
  }

  void Greet(const proto::greeting::Request& request,
          Response<proto::greeting::Response>& response) {
    TIMBER_LOG_INFO("Greeting request from {}", request.name());

    {
      timber::trace::ScopedSpan greet("Greet");

      response->set_greeting(
          fmt::format("Hello, {}!", request.name()));

      TIMBER_LOG_INFO("Work done");
    }
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

 private:
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

class ProxyService final :
    public rpc::ServiceBase<ProxyService> {
 public:
  static constexpr std::string_view kName = "Proxy";

  ProxyService()
      : logger_(kName),
        backend_(MakeBackendChannel()) {
  }

  void Greet(const proto::greeting::Request& request,
          Response<proto::greeting::Response>& response) {

    TIMBER_LOG_INFO("Forward request to Greeting service");

    auto future = rpc::Call("Greeting.Greet")
        .Request(request)
        .Via(backend_)
        .Done()
        .As<proto::greeting::Response>();

    response.Set(std::move(future));
  }

 private:
  rpc::IChannelPtr MakeBackendChannel() {
    auto dialer = rpc::MakeDialer();

    auto socket = dialer->Dial(kServerPort);
    return rpc::channels::Trace(std::move(socket));
  }

  void RegisterMethods() {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

 private:
  timber::log::Logger logger_;
  rpc::IChannelPtr backend_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto stack = [] {
    auto slowpoke = rpc::Make<GreetingService>();
    auto proxy = rpc::Make<ProxyService>();

    auto router = rpc::filters::Route();

    router->Add(std::move(slowpoke));
    router->Add(std::move(proxy));

    return rpc::filters::Furnish(std::move(router));
  }();

  server->Serve(std::move(stack));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client() {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    // Make socket channel
    auto socket = dialer->Dial(kServerPort);

    // Trace request
    auto trace = rpc::channels::Trace(std::move(socket));

    return trace;
  }();

  proto::greeting::Request hi;
  hi.set_name("Morty");

  auto future = rpc::Call("Proxy.Greet")
      .Request(hi)
      .Via(stack)
      .Done()
      .As<proto::greeting::Response>();

  auto result = await::Await(std::move(future));

  assert(result.IsOk());

  TIMBER_LOG_INFO("Greeting: {}", result->greeting());

  TIMBER_LOG_INFO("Client completed");
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("TracingExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
