#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/filters/furnish.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/channels/failsafe/circuit_breaker.hpp>
#include <commute/rpc/channels/failsafe/timeout.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/channels/balance/rr.hpp>
#include <commute/rpc/channels/balance/random.hpp>
#include <commute/rpc/channels/balance/p2c.hpp>

#include <commute/rpc/balance/load/ema.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/await.hpp>

#include <timber/log/log.hpp>

#include <wheels/core/defer.hpp>

#include "proto/cpp/resource.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class ResourceService final :
    public rpc::ServiceBase<ResourceService> {
 public:
  static constexpr std::string_view kName = "Resource";

  explicit ResourceService(std::chrono::milliseconds delay)
      : delay_(delay),
        logger_(kName) {
  }

  void Use(const proto::resource::Request& /*request*/,
           Response<proto::resource::Response>& /*response*/) {
    commute::fibers::SleepFor(delay_);
    TIMBER_LOG_INFO("Use completed");
  }

 private:

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Use);
  }

 private:
  const std::chrono::milliseconds delay_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server(std::string port, bool slow) {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(port);

  auto resource = rpc::Make<ResourceService>(slow ? 3s : 100ms);

  server->Serve(rpc::filters::Furnish(std::move(resource)));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

class ReplicaClient : public rpc::IClient {
 public:
  ReplicaClient()
    : dialer_(rpc::MakeDialer()) {
  }

  rpc::IChannelPtr Dial(const std::string& addr) override {
    auto socket = dialer_->Dial(addr);

    auto timeout = rpc::channels::Timeout(
        std::move(socket),
        rpc::channels::TimeoutParams()
            .Value(200ms));

    auto failfast = rpc::channels::CircuitBreaker(
        std::move(timeout),
        rpc::channels::CircuitBreakerParams()
            .MaxFailures(5)
            .ResetTimeout(10s)
            .SuccessThreshold(1));

    return failfast;
  }

 private:
  rpc::IClientPtr dialer_;
};

auto ClientStack(std::vector<std::string> addrs) {
  auto dialer = std::make_shared<ReplicaClient>();

  auto ema_params = rpc::balance::load::EmaParams()
      .Window(4s);

  auto balancer = rpc::channels::PowerOf2Choices(
      rpc::Static(addrs),
      dialer,
      rpc::balance::load::EmaRtt(ema_params));

  // Retries
  auto backoff = rpc::retry::backoff::WithJitter(
      rpc::retry::backoff::Exponential(
          rpc::retry::backoff::ExponentialParams()
              .Init(50ms)
              .Max(5s)
              .Factor(2)),
      rpc::retry::jitter::Equal());

  auto retry = rpc::channels::Retry(
      std::move(balancer),
      backoff);

  return retry;
}

//////////////////////////////////////////////////////////////////////

void Client(std::vector<std::string> ports) {
  timber::log::Logger logger_("Client");

  auto stack = ClientStack(ports);

  // Call

  for (size_t i = 0; i < 128; ++i) {
    // Request message
    proto::resource::Request request;

    auto future = rpc::Call("Resource.Use")
        .Request(request)
        .Via(stack)
        .Done()
        .As<proto::resource::Response>();

    await::Await(std::move(future)).ThrowIfError();
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("CircuitBreakerExample");

  sim.LogBackendImpl()
    .SetDefaultLevel(timber::log::Level::Debug)
    .SetComponentLevel("Circuit-Breaker", timber::log::Level::Debug);

  sim.Spawn([] {
    Server(/*port=*/ "123", /*slow=*/ true);
  }).Spawn([] {
    Server(/*port=*/ "124", /*slow=*/ false);
  }).Spawn([] {
    Client(std::vector<std::string>({"123", "124"}));
  });

  sim.Run();

  return 0;
}
