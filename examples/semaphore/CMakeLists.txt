set(EXAMPLE_TARGET commute_example_semaphore)

add_subdirectory(proto)

add_executable(${EXAMPLE_TARGET} main.cpp)
target_link_libraries(${EXAMPLE_TARGET} commute commute_example_semaphore_protos)
