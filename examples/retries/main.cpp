#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/log.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>

#include <fallible/error/make.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/resource.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class ResourceService final :
    public rpc::ServiceBase<ResourceService> {
 public:
  static constexpr std::string_view kName = "Resource";

  explicit ResourceService(size_t fails)
      : fails_(fails),
        logger_(kName) {
  }

  void Use(const proto::resource::Request& /*request*/,
           Response<proto::resource::Response>& response) {

    if (fails_ > 0) {
      --fails_;
      // Transient failure
      response.Fail(Unavailable());
    }

    DoWork();
    // Success
  }

 private:
  void DoWork() {
    commute::fibers::SleepFor(1s);
  }

  static fallible::Error Unavailable() {
    return fallible::errors::Unavailable()
        .Domain("Resource")
        .Reason("Some transient error")
        .Done();
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Use);
  }

 private:
  size_t fails_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(/*port=*/kServerPort);

  auto stack = [] {
    // Log -> Resource
    auto resource = rpc::Make<ResourceService>(/*tries=*/3);
    return rpc::filters::Log(std::move(resource));
  }();

  server->Serve(std::move(stack));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

// Backoff strategy for retry

rpc::retry::IBackoffPtr Backoff() {
  // return rpc::retry::backoff::Fixed(2s);
  // return rpc::retry::backoff::Exponential({1s, 10s, 2});
  return rpc::retry::backoff::WithJitter(
      rpc::retry::backoff::Exponential({1s, 10s, 2}),
      rpc::retry::jitter::Equal());
}

void Client() {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(/*address=*/kServerPort);

    auto retry = rpc::channels::Retry(
        std::move(socket),
        Backoff());

    return retry;
  }();

  proto::resource::Request req;

  auto future = rpc::Call("Resource.Use")
      .Request(req)
      .Via(stack)
      .Done()
      .As<proto::resource::Response>();

  await::Await(std::move(future)).ThrowIfError();
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("RetriesExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
