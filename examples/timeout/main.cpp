#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/channels/failsafe/timeout.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>

#include <commute/rpc/channels/observe/trace.hpp>

#include <commute/rpc/filters/route.hpp>
#include <commute/rpc/filters/deadline.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/await.hpp>

#include <timber/log/log.hpp>

#include <wheels/core/defer.hpp>

#include "proto/cpp/slowpoke.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class SlowpokeService final :
    public rpc::ServiceBase<SlowpokeService> {
 public:
  static constexpr std::string_view kName = "Slowpoke";

  SlowpokeService()
      : logger_(kName) {
  }

  void Greet(const proto::slowpoke::Request& /*request*/,
          Response<proto::slowpoke::Response>& /*response*/) {
    wheels::Defer note([this]() {
      TIMBER_LOG_INFO("Request cancelled");
    });

    while (true) {
      TIMBER_LOG_INFO("Zzz...");
      commute::fibers::SleepFor(1s);  // <- Cancellation checkpoint
    }
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

 private:
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

class ProxyService final :
    public rpc::ServiceBase<ProxyService> {
 public:
  static constexpr std::string_view kName = "Proxy";

  ProxyService()
      : logger_(kName),
        backend_(MakeBackendChannel()) {
  }

  void Greet(const proto::slowpoke::Request& request,
          Response<proto::slowpoke::Response>& response) {

    wheels::Defer note([this]() {
      TIMBER_LOG_INFO("Request cancelled");
    });

    TIMBER_LOG_INFO("Forward request to Slowpoke");

    auto future = rpc::Call("Slowpoke.Greet")
        .Request(request)
        .Via(backend_)
        .Done()
        .As<proto::slowpoke::Response>();

    auto result = await::Await(std::move(future));

    response.Set(std::move(result));
  }

 private:
  rpc::IChannelPtr MakeBackendChannel() {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(kServerPort);

    auto trace = rpc::channels::Trace(std::move(socket));

    return trace;
  }

  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

 private:
  timber::log::Logger logger_;
  rpc::IChannelPtr backend_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto stack = [] {
    auto slowpoke = rpc::Make<SlowpokeService>();
    auto proxy = rpc::Make<ProxyService>();

    auto router = rpc::filters::Route();

    router->Add(std::move(slowpoke));
    router->Add(std::move(proxy));

    auto deadline = rpc::filters::DeadLine(std::move(router));

    auto log = rpc::filters::Log(std::move(deadline));

    return log;
  }();

  server->Serve(std::move(stack));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client() {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    // Trace -> Timeout -> Retries -> Socket

    auto dialer = rpc::MakeDialer();

    // Make socket channel
    auto socket = dialer->Dial(kServerPort);

    // Add retry
    auto retry = rpc::channels::Retry(
        std::move(socket),
        rpc::retry::backoff::Exponential({100ms, 2s, 2}));

    // Support timeout / deadline propagation
    auto timeout = rpc::channels::Timeout(std::move(retry));

    // Trace request
    auto trace = rpc::channels::Trace(std::move(timeout));

    return trace;
  }();

  proto::slowpoke::Request hi;

  auto future = rpc::Call("Proxy.Greet")
      .Request(hi)
      .Via(stack)
      .Timeout(5s)  // <-- Set `timeout.ms` parameter
      .Done()
      .As<proto::slowpoke::Response>();

  // Future -> Result
  auto result = await::Await(std::move(future));

  assert(result.Failed());

  TIMBER_LOG_INFO("Request failed: {}", result.Error().Describe());

  TIMBER_LOG_INFO("Client completed");
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("TimeoutExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
