#pragma once

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <ticktock/clocks/wall_time.hpp>

namespace fmt {

template <>
struct formatter<ticktock::WallTime> : ostream_formatter {};

}  // namespace fmt
