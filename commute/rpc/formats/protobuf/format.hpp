#pragma once

#include <commute/concurrency/futures/types/boxed.hpp>

#include <commute/rpc/core/request/message.hpp>
#include <commute/rpc/errors/domain.hpp>

#include <pickle/serialize.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

#include <await/futures/types/boxed.hpp>
#include <await/futures/combine/seq/and_then.hpp>

#include <string>

namespace commute::rpc::formats::protobuf {

//////////////////////////////////////////////////////////////////////

namespace detail {

template<typename TMessage>
fallible::Result<TMessage> TryFromBytes(const std::string& bytes) {
  TMessage message;

  if (pickle::Deserialize(bytes, &message)) {
    return fallible::Ok(std::move(message));
  } else {
    return fallible::Fail(
        fallible::errors::Invalid()
            .Domain(rpc::errors::Domain())
            .Reason("Cannot deserialize Protobuf message")  // TODO: message type
            .Done()
    );
  }
}

//////////////////////////////////////////////////////////////////////

struct AsResponse {
 public:
  AsResponse(BoxedFuture<ByteMessage> raw) : raw_(std::move(raw)) {
  }

  template <typename TMessage>
  Future<TMessage> auto As() && {
    return std::move(raw_) |
      await::futures::AndThen([](ByteMessage bytes) {
        return detail::TryFromBytes<TMessage>(bytes);
      });
  }

  // Implicit cast
  template <typename TMessage>
  operator BoxedFuture<TMessage>()&& {
    return std::move(*this).As<TMessage>();
  }

  BoxedFuture<ByteMessage> Raw() {
    return std::move(raw_);
  }

 private:
  BoxedFuture<ByteMessage> raw_;
};

}  // namespace detail

//////////////////////////////////////////////////////////////////////

struct Format {
  template <typename TMessage>
  static std::string ToBytes(const TMessage& message) {
    return pickle::Serialize(message);
  }

  template <typename TMessage>
  static fallible::Result<TMessage> TryFromBytes(const std::string& bytes) {
    return detail::TryFromBytes<TMessage>(bytes);
  }

  static auto FromFutureBytes(BoxedFuture<std::string> bytes) {
    return detail::AsResponse{std::move(bytes)};
  }
};

}  // namespace commute::rpc::formats::protobuf
