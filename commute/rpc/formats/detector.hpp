#pragma once

#include <commute/rpc/formats/protobuf/format.hpp>

namespace commute::rpc::formats {

// Default: Protobuf

template <typename TMessage>
struct FormatDetector {
  using Format = protobuf::Format;
};

}  // namespace commute::rpc::formats
