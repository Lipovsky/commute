#include <commute/rpc/health/health.hpp>

#include <commute/rpc/core/service/service_base.hpp>

#include <commute/rpc/health/proto/cpp/health.pb.h>

namespace commute::rpc {

class HealthService : public ServiceBase<HealthService> {
 public:
  static constexpr const char* kName = "Health";

  void Ping(const proto::commute::rpc::health::Ping& /*request*/,
            Response<proto::commute::rpc::health::Pong>& /*response*/) {
    return;  // Ok
  }

 private:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }
};

IServicePtr MakeHealthService() {
  return std::make_shared<HealthService>();
}

}  // namespace commute::rpc
