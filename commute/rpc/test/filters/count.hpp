#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::test::filters {

//////////////////////////////////////////////////////////////////////

struct ICounter : IEndpoint {
  virtual size_t Count() const = 0;
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<ICounter> Count(IEndpointPtr endpoint);

}  // namespace commute::rpc::test::filters
