#include <commute/rpc/test/channels/count.hpp>

#include <await/futures/combine/seq/anyway.hpp>

#include <atomic>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

 class Counter :
     public ICounter,
     public std::enable_shared_from_this<Counter> {
 public:
  Counter(IChannelPtr channel)
      : channel_(std::move(channel)) {
  }

  // IChannel

  BoxedFuture<ByteMessage> Call(Request request) override {
    return channel_->Call(std::move(request))
        | await::futures::Anyway([self = shared_from_this(), this] {
            ++count_;
          });
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    channel_->Close();
  }

  // ICounter

  size_t Count() const override {
    return count_.load();
  }

 private:
  IChannelPtr channel_;

  std::atomic<size_t> count_{0};
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<ICounter> Count(IChannelPtr channel) {
  return std::make_shared<Counter>(channel);
}

}  // namespace commute::rpc::test::channels
