#include <commute/rpc/test/channels/delay.hpp>

#include <commute/rt/locate/timers.hpp>

#include <await/futures/make/after.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <chrono>

namespace commute::rpc::test::channels {

class DelayChannel : public IChannel {
 public:
  DelayChannel(IChannelPtr channel, std::chrono::milliseconds delay)
    : channel_(std::move(channel)), delay_(delay) {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    return await::futures::After(rt::Timers().Delay(delay_))
              >> channel_->Call(std::move(request));
  }

  const std::string& Peer() const override {
    static const std::string kName = "Echo";
    return kName;
  }

  live::Status Status() override {
    return live::Status::Alive;
  }

  void Close() override {
    // No-op
  }

 private:
  IChannelPtr channel_;
  std::chrono::milliseconds delay_;
};

IChannelPtr Delay(IChannelPtr channel,
                  std::chrono::milliseconds millis) {
  return std::make_shared<DelayChannel>(std::move(channel), millis);
}

}  // namespace commute::rpc::test::channels
