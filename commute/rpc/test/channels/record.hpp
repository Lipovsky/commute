#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <ticktock/clocks/wall_time.hpp>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

using Timestamp = ticktock::WallTime;

//////////////////////////////////////////////////////////////////////

struct IRecorder : IChannel {
  virtual size_t CallCount() const = 0;
  virtual std::vector<Timestamp> CallTimestamps() const = 0;
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<IRecorder> Record(IChannelPtr channel);

}  // namespace commute::rpc::test::channels
