#pragma once

#include <commute/rpc/core/client/channel.hpp>

namespace commute::rpc::test::channels {

IChannelPtr Echo();

}  // namespace commute::rpc::test::channels
