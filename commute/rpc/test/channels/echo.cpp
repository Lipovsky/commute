#include <commute/rpc/test/channels/echo.hpp>

#include <await/futures/make/value.hpp>

namespace commute::rpc::test::channels {

class EchoChannel : public IChannel {
 public:
  BoxedFuture<ByteMessage> Call(Request request) override {
    return await::futures::Value(fallible::Ok(request.body));
  }

  const std::string& Peer() const override {
    static const std::string kName = "Echo";
    return kName;
  }

  live::Status Status() override {
    return live::Status::Alive;
  }

  void Close() override {
    // No-op
  }
};

IChannelPtr Echo() {
  return std::make_shared<EchoChannel>();
}

}  // namespace commute::rpc::test::channels
