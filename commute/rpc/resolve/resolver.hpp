#pragma once

#include <vector>
#include <string>

namespace commute::rpc {

using Addrs = std::vector<std::string>;

struct IResolver {
  virtual ~IResolver() = default;

  // List replicas
  virtual Addrs Resolve() = 0;
};

using IResolverPtr = std::shared_ptr<IResolver>;

}  // namespace commute::rpc
