#include <commute/rpc/resolve/dynamic.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

class List : public IResolver {
 public:
  List(Addrs addrs)
      : addrs_(std::move(addrs)) {
  }

  Addrs Resolve() override {
    return addrs_;
  }

 private:
  const Addrs addrs_;
};

//////////////////////////////////////////////////////////////////////

IResolverPtr Static(Addrs addrs) {
  return std::make_shared<List>(std::move(addrs));
}

}  // namespace commute::rpc
