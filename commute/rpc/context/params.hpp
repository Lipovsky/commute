#pragma once

#include <carry/kv.hpp>

#include <fmt/core.h>

namespace commute::rpc::params {

inline carry::Key ContextKey(std::string_view name) {
  return ::fmt::format("commute.rpc.param.{}", name);
}

}  // namespace commute::rpc::params
