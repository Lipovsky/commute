#include <commute/rpc/client/dialer.hpp>

#include <commute/rpc/proto/jet/client/socket.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

class Dialer : public IClient {
 public:
  IChannelPtr Dial(const std::string& peer) override {
    return std::make_shared<jet::SocketChannel>(peer);
  }
};

//////////////////////////////////////////////////////////////////////

IClientPtr MakeDialer() {
  return std::make_shared<Dialer>();
}

}  // namespace commute::rpc
