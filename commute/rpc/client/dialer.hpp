#pragma once

#include <commute/rpc/core/client/client.hpp>

namespace commute::rpc {

IClientPtr MakeDialer();

}  // namespace commute::rpc
