#pragma once

#include <string>

namespace commute::rpc {

// Serialized request | response
using ByteMessage = std::string;

}  // namespace commute::rpc
