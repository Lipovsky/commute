#include <commute/rpc/core/request/method.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/string_utils.hpp>

#include <fmt/core.h>

namespace commute::rpc {

std::string Method::FullName() {
  return fmt::format("{}.{}", service, name);
}

Method Method::Parse(const std::string& method) {
  auto parts = wheels::Split(method, '.');
  WHEELS_VERIFY(parts.size() == 2,
                "Invalid method format: '"
                    << method << "', expected {service}.{method_name}");
  return {parts[0], parts[1]};
}

std::ostream& operator<<(std::ostream& out, const Method& method) {
  out << method.service << "." << method.name;
  return out;
}

}  // namespace commute::rpc
