#pragma once

#include <commute/rpc/core/request/method.hpp>
#include <commute/rpc/core/request/message.hpp>

#include <carry/context.hpp>

namespace commute::rpc {

struct Request {
  Method method;
  ByteMessage body;
  carry::Context context;
};

}  // namespace commute::rpc
