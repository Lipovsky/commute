#pragma once

#include <string>
#include <ostream>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace commute::rpc {

struct Method {
  std::string service;
  std::string name;

  // Format: "{service}.{name}"
  static Method Parse(const std::string& method);

  std::string FullName();
};

std::ostream& operator<<(std::ostream& out, const Method& method);

}  // namespace commute::rpc

namespace fmt {

template <>
struct formatter<commute::rpc::Method> : ostream_formatter {};

}  // namespace fmt
