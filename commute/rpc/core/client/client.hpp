#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <memory>

namespace commute::rpc {

struct IClient {
  virtual ~IClient() = default;

  virtual IChannelPtr Dial(const std::string& peer) = 0;
};

using IClientPtr = std::shared_ptr<IClient>;

}  // namespace commute::rpc
