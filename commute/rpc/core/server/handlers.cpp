#include <commute/rpc/core/server/handlers.hpp>

#include <commute/rpc/core/server/metadata.hpp>
#include <commute/rpc/context/headers.hpp>

#include <commute/rt/locate/executor.hpp>

#include <commute/concurrency/futures/types/boxed.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/contract.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/clone.hpp>
#include <await/futures/combine/seq/unit.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/run/go.hpp>

#include <await/tasks/exe/strand.hpp>

#include <timber/log/log.hpp>

#include <map>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

using Handler = BoxedFuture<wheels::Unit>;

//////////////////////////////////////////////////////////////////////

// Not thread-safe!

class HandlerRegistry {
 public:
  HandlerRegistry()
    : logger_("Handler-Manager") {
  }

  void Remove(RequestId id) {
    TIMBER_LOG_DEBUG("Remove request-id = {}", id);
    if (auto it = handlers_.find(id); it != handlers_.end()) {
      handlers_.erase(it);
    }
  }

  void Add(RequestId id, Handler handler) {
    TIMBER_LOG_DEBUG("Register request-id = {}", id);
    handlers_.insert({id, std::move(handler)});
  }

  void Cancel(RequestId id) {
    if (auto it = handlers_.find(id); it != handlers_.end()) {
      TIMBER_LOG_DEBUG("Send cancellation request to handler for request-id = {}", id);
      handlers_.erase(it);
    } else {
      TIMBER_LOG_DEBUG("request-id = {} not found", id);
    }
  }

  void CancelAll() {
    TIMBER_LOG_INFO("Cancel all active requests: {}", handlers_.size());
    handlers_.clear();
  }

 private:
  std::map<std::string, Handler> handlers_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

class HandlerManager :
    public IHandlerManager,
    public std::enable_shared_from_this<HandlerManager> {
 public:
  HandlerManager(IEndpointPtr endpoint)
    : endpoint_(std::move(endpoint)),
      executor_(rt::Executor()),
      strand_(executor_),
      handlers_(),
      logger_("Handler-Manager") {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto self = shared_from_this();

    auto cancel = headers::TryGet(request.context, "cancel");

    const auto meta = GetMetadata(request.context);

    if (cancel && cancel->AsBool()) {
      TIMBER_LOG_DEBUG("Try to cancel request with request-id = {}", meta.id);

      await::futures::Submit(strand_, [self, this, id = meta.id]() {
        handlers_.Cancel(id);
      }) | await::futures::Go();

      return IgnoreMe();
    }

    auto [left, right] = endpoint_->Call(std::move(request)) |
        await::futures::Clone();

    {
      // Right
      // Register handler

      auto handler = std::move(right) | await::futures::ToUnit();

      // Already scheduled to strand!
      await::futures::Submit(strand_, [self, this, id = meta.id, handler = std::move(handler)]() mutable {
        handlers_.Add(id, std::move(handler));
      }) | await::futures::Go();
    }

    {
      // Left
      // Propagate call upstream

      // NB: Remove happens-after Add
      return std::move(left) |
          await::futures::Via(strand_) |
          await::futures::Anyway([self, this, id = meta.id]() {
            handlers_.Remove(id);
          }) |
          await::futures::Via(executor_);
    }
  }

  const std::string& Name() const override {
    return endpoint_->Name();
  }

  void CancelAll() override {
    await::futures::Submit(strand_, [self = shared_from_this(), this]() {
      handlers_.CancelAll();
    }) | await::futures::Go();
  }

 private:
  BoxedFuture<ByteMessage> IgnoreMe() {
    auto [f, p] = await::futures::Contract<fallible::Result<ByteMessage>>();
    std::move(p).Cancel();
    return std::move(f);
  }

 private:
  IEndpointPtr endpoint_;

  // Runtime
  await::tasks::IExecutor& executor_;

  await::tasks::Strand strand_;
  HandlerRegistry handlers_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IHandlerManagerPtr ManageHandlers(IEndpointPtr endpoint) {
  return std::make_shared<HandlerManager>(
      std::move(endpoint));
}

}  // namespace commute::rpc
