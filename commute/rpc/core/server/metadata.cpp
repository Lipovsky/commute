#include <commute/rpc/core/server/metadata.hpp>

namespace commute::rpc {

const carry::Key& MetadataKey() {
  static const std::string kMetadataKey = "commute.rpc.metadata";
  return kMetadataKey;
}

Metadata GetMetadata(const carry::Context& context) {
  return context.Get<Metadata>(MetadataKey());
};

}  // namespace commute::rpc
