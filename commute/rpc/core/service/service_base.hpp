#pragma once

#include <commute/rpc/core/service/service.hpp>
#include <commute/rpc/core/service/register_method.hpp>
#include <commute/rpc/core/service/response.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/rpc/formats/detector.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <await/tasks/curr/cancelled.hpp>

#include <fallible/error/make.hpp>
#include <fallible/result/make.hpp>

#include <wheels/core/assert.hpp>

#include <fmt/core.h>

#include <map>

namespace commute::rpc {

template <typename TService>
class ServiceBase : public IService {
 protected:
  using ThisService = TService;

  template <typename TMessage>
  using Response = Response<typename formats::FormatDetector<TMessage>::Format, TMessage>;

 public:
  ServiceBase()
    : name_(TService::kName) {
  }

  const std::string& Name() const override {
    return name_;
  }

  bool Has(const std::string& method_name) const override {
    return methods_.find(method_name) != methods_.end();
  }

  BoxedFuture<ByteMessage> Invoke(const std::string& method_name,
                                             const ByteMessage& input) override {
    auto method_it = methods_.find(method_name);
    WHEELS_VERIFY(method_it != methods_.end(),
                  "RPC method not found: " << method_name);

    auto& invoker = method_it->second;
    return invoker(input);
  }

  void Initialize() override {
    RegisterMethods();
  }

 protected:
  // Override this
  // Use COMMUTE_RPC_REGISTER_METHOD to register methods
  virtual void RegisterMethods() = 0;

  template <typename TRequestMessage, typename TResponseMessage>
  void RegisterMethod(const std::string& method_name,
                      void (TService::*method)(const TRequestMessage&,
                                               Response<TResponseMessage>&)) {
    using Format = typename formats::FormatDetector<TRequestMessage>::Format;

    TService* self = static_cast<TService*>(this);

    auto closure = [self, method](
                       const TRequestMessage& request,
                       Response<TResponseMessage>& response) -> void {
      (self->*method)(request, response);
    };

    auto invoker =
        [closure = std::move(closure)](
            const ByteMessage& input) mutable -> BoxedFuture<ByteMessage> {

      // Deserialize
      fallible::Result<TRequestMessage> request = Format::template TryFromBytes<TRequestMessage>(input);

      if (!request.IsOk()) {
        return futures::Fail<ByteMessage>(request.Error());
      }

      Response<TResponseMessage> response;

      // Invoke service method

      try {
        closure(*request, response);
      }
      catch (await::tasks::curr::CancelledException&) {
        // Ignore cancellation
        throw;
      }
      catch (...) {
        return futures::Fail<ByteMessage>(
            fallible::errors::Unknown()
              .Domain(rpc::errors::Domain())
              .Reason(::fmt::format("Unhandled exception",
                                  wheels::CurrentExceptionMessage()))
              .Done());
      }

      return response.ToFuture();
    };

    methods_.emplace(method_name, invoker);
  }

 private:
  const std::string name_;

  using MethodInvoker = std::function<BoxedFuture<ByteMessage>(const ByteMessage&)>;
  // Name -> Invoker
  std::map<std::string, MethodInvoker> methods_;
};

}  // namespace commute::rpc
