#pragma once

#include <commute/rpc/core/request/message.hpp>
#include <commute/rpc/context/headers.hpp>

#include <commute/concurrency/futures/make/fail.hpp>
#include <commute/concurrency/futures/types/boxed.hpp>

#include <await/futures/make/value.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/with.hpp>

#include <carry/new.hpp>

#include <fallible/result/make.hpp>

#include <wheels/core/panic.hpp>
#include <wheels/core/assert.hpp>

#include <string>
#include <optional>

namespace commute::rpc {

template <typename Format, typename TMessage = typename Format::MessageType>
class Response {
  using Storage = std::variant<
      TMessage,
      fallible::Error,
      fallible::Result<TMessage>,
      BoxedFuture<ByteMessage>>;

 public:
  Response()
    : context_(carry::Empty()) {
  }

  // Message field access:
  // response_object->message_field
  TMessage* operator->() {
    ExpectNotSet();
    return &std::get<0>(storage_);
  }

  void Fail(fallible::Error error) {
    ExpectNotSet();
    storage_ = std::move(error);
  }

  void Set(fallible::Result<TMessage> result) {
    ExpectNotSet();
    storage_ = std::move(result);
  }

  void Set(BoxedFuture<TMessage> future) {
    ExpectNotSet();
    storage_ = std::move(future) | await::futures::Map([](TMessage rsp) {
      return Format::ToBytes(rsp);
    });
  }

  void Set(BoxedFuture<ByteMessage> raw_future) {
    ExpectNotSet();
    storage_ = std::move(raw_future);
  }

  void SetHeader(std::string name, rpc::headers::Value value) {
    context_.Set(rpc::headers::ContextKey(name), std::move(value));
  }

  void SetHeaders(const carry::Context& ctx) {
    auto headers = rpc::headers::List(ctx);
    for (auto h : headers) {
      SetHeader(h.name, h.value);
    }
  }

  BoxedFuture<ByteMessage> ToFuture() {
    auto f = StorageToFuture();
    // TODO: Context
    // context_.ReWrap(f.UserContext());
    return std::move(f);
    // await::futures::With(context_.Done());
  }

 private:
  BoxedFuture<ByteMessage> StorageToFuture() {
    if (storage_.index() == 0) {
      // TMessage
      return await::futures::Value(fallible::Ok(Format::ToBytes(std::get<0>(storage_))));
    } else if (storage_.index() == 1) {
      // Error
      return commute::futures::Fail<ByteMessage>(std::move(std::get<1>(storage_)));
    } else if (storage_.index() == 2) {
      // Result
      auto result = std::move(std::get<2>(storage_)).Map([](TMessage rsp) {
        return Format::ToBytes(std::move(rsp));
      });
      return await::futures::Value(std::move(result));
    } else if (storage_.index() == 3) {
      // Future
      return std::move(std::move(std::get<3>(storage_)));
    } else {
      std::abort();  // Logic error
    }
  }

  void ExpectNotSet() const {
    WHEELS_ASSERT(storage_.index() == 0, "Response already set as failed");
  }

 private:
  Storage storage_;
  carry::ReWrapper context_;
};

}  // namespace commute::rpc
