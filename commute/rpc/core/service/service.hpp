#pragma once

#include <commute/rpc/core/request/message.hpp>

#include <commute/concurrency/futures/types/boxed.hpp>

#include <memory>
#include <string>

namespace commute::rpc {

struct IService {
  virtual ~IService() = default;

  virtual void Initialize() = 0;

  virtual const std::string& Name() const = 0;

  virtual bool Has(const std::string& method) const = 0;

  virtual BoxedFuture<ByteMessage> Invoke(
      const std::string& method,
      const ByteMessage& request) = 0;
};

using IServicePtr = std::shared_ptr<IService>;

}  // namespace commute::rpc
