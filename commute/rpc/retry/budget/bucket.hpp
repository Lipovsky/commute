#pragma once

#include <commute/rpc/retry/budget.hpp>

#include <cstdint>

namespace commute::rpc::retry::budget {

//////////////////////////////////////////////////////////////////////

using Tokens = uint64_t;

struct BucketParams {
  static const Tokens kDefaultLimit = 100;

  Tokens limit = kDefaultLimit;
  Tokens init = kDefaultLimit;

  // <= 10% retries
  Tokens deposit = 1;
  Tokens withdraw = 100;

  using Params = BucketParams;

  Params& Limit(Tokens count) {
    limit = count;
    return *this;
  }

  Params& Init(Tokens count) {
    init = count;
    return *this;
  }

  Params& Deposit(Tokens count) {
    deposit = count;
    return *this;
  }

  Params& Withdraw(Tokens count) {
    withdraw = count;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IBudgetPtr Bucket(BucketParams params);

}  // namespace commute::rpc::retry::budget
