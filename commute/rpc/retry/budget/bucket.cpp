#include <commute/rpc/retry/budget/bucket.hpp>

#include <twist/ed/stdlike/atomic.hpp>

#include <algorithm>

namespace commute::rpc::retry::budget {

//////////////////////////////////////////////////////////////////////

class BucketBudget final : public IBudget {
 public:
  BucketBudget(BucketParams params)
    : params_(params),
      tokens_(params.init) {
  }

  bool TryWithdraw() override {
    Tokens curr = tokens_.load();

    while (true) {
      if (curr < params_.withdraw) {
        return false;
      }

      Tokens next = curr - params_.withdraw;

      if (tokens_.compare_exchange_weak(curr, next)) {
        return true;;
      } else {
        // curr updated
      }
    }
  }

  void Deposit() override {
    Tokens curr = tokens_.load();

    while (true) {
      Tokens next = std::min(curr + params_.deposit, params_.limit);

      if (tokens_.compare_exchange_weak(curr, next)) {
        return;
      } else {
        // curr updated
      }
    }
  }

 private:
  const BucketParams params_;
  twist::ed::stdlike::atomic<Tokens> tokens_;
};

//////////////////////////////////////////////////////////////////////

IBudgetPtr Bucket(BucketParams params) {
  return std::make_shared<BucketBudget>(params);
}

}  // namespace commute::rpc::retry::budget
