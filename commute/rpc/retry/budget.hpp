#pragma once

#include <memory>

namespace commute::rpc::retry {

struct IBudget {
  virtual ~IBudget() = default;

  // Ask for retry permit
  virtual bool TryWithdraw() = 0;

  // Report completed request
  virtual void Deposit() = 0;
};

using IBudgetPtr = std::shared_ptr<IBudget>;

}  // namespace commute::rpc::retry
