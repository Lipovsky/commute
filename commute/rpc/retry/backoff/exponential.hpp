#pragma once

#include <commute/rpc/retry/backoff.hpp>

#include <snowflake/random/generator.hpp>

namespace commute::rpc::retry::backoff {

//////////////////////////////////////////////////////////////////////

// Exponential backoff
// https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/

//////////////////////////////////////////////////////////////////////

struct ExponentialParams {
  std::chrono::milliseconds init;
  std::chrono::milliseconds max;
  size_t factor;

  using Params = ExponentialParams;

  // Build

  Params& Init(std::chrono::milliseconds delay) {
    init = delay;
    return *this;
  }

  Params& Max(std::chrono::milliseconds delay) {
    max = delay;
    return *this;
  }

  Params& Factor(size_t value) {
    factor = value;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IBackoffPtr Exponential(ExponentialParams params);

}  // namespace commute::rpc::retry::backoff
