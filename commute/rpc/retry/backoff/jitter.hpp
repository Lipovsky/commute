#pragma once

#include <commute/rpc/retry/backoff.hpp>

// Jitter strategies
#include <commute/rpc/retry/jitter/equal.hpp>
#include <commute/rpc/retry/jitter/full.hpp>

namespace commute::rpc::retry::backoff {

IBackoffPtr WithJitter(IBackoffPtr backoff, IJitterPtr jitter);

}  // namespace commute::rpc::retry::backoff
