#pragma once

#include <commute/rpc/retry/backoff.hpp>

namespace commute::rpc::retry::backoff {

IBackoffPtr Fixed(std::chrono::milliseconds delay);

}  // namespace commute::rpc::retry::backoff
