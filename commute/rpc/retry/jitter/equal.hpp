#pragma once

#include <commute/rpc/retry/jitter.hpp>

namespace commute::rpc::retry::jitter {

// [delay/2, delay]

IJitterPtr Equal();

// For unit testing
IJitterPtr Equal(snowflake::random::IGenerator& random);

}  // namespace commute::rpc::retry::jitter
