#pragma once

#include <commute/rpc/retry/jitter.hpp>

namespace commute::rpc::retry::jitter {

// [0, delay]

IJitterPtr Full();

// For unit testing
IJitterPtr Full(snowflake::random::IGenerator& random);

}  // namespace commute::rpc::retry::jitter
