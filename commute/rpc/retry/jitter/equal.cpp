#include <commute/rpc/retry/jitter/equal.hpp>

#include <commute/rt/locate/random.hpp>

using Millis = std::chrono::milliseconds;

namespace commute::rpc::retry::jitter {

//////////////////////////////////////////////////////////////////////

class EqualJitter : public IJitter {
 public:
  EqualJitter(snowflake::random::IGenerator& random)
      : random_(random) {
  }

  // IJitter
  Millis Add(Millis delay) override {
    auto units = delay.count();
    return Millis(Random(units / 2, units));
  }

  uint64_t Random(uint64_t lo, uint64_t hi) {
    return random_.Jitter(hi - lo) + lo;
  }

 private:
  snowflake::random::IGenerator& random_;
};

//////////////////////////////////////////////////////////////////////

IJitterPtr Equal(snowflake::random::IGenerator& random) {
  return std::make_shared<EqualJitter>(random);
}

IJitterPtr Equal() {
  return Equal(rt::Random());
}

}  // namespace commute::rpc::retry::jitter
