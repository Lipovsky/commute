#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::filters {

// Add common filters (trace, log, etc) to service / router endpoint
IEndpointPtr Furnish(IEndpointPtr raw);

}  // namespace commute::rpc::filters
