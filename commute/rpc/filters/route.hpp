#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

struct IRouter : IEndpoint {
  virtual void Add(IEndpointPtr endpoint) = 0;
};

using IRouterPtr = std::shared_ptr<IRouter>;

//////////////////////////////////////////////////////////////////////

IRouterPtr Route(IEndpointPtr or_continue);

IRouterPtr Route();

IEndpointPtr Intercept(IEndpointPtr intercept,
                       IEndpointPtr or_continue);

}  // namespace commute::rpc::filters
