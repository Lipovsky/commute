#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::filters {

IEndpointPtr Log(IEndpointPtr endpoint);

}  // namespace commute::rpc::filters
