#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

struct SemaphoreParams {
  size_t permits = 1;
  size_t queue_limit = 0;

  using Params = SemaphoreParams;

  // Should be > 0
  Params& ConcurrencyLimit(size_t count) {
    assert(count > 0);
    permits = count;
    return *this;
  }

  Params& QueueLimit(size_t count) {
    queue_limit = count;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr Semaphore(IEndpointPtr endpoint,
                       SemaphoreParams params);

}  // namespace commute::rpc::filters
