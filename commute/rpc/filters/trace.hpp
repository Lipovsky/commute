#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc::filters {

IEndpointPtr Trace(IEndpointPtr endpoint);

}  // namespace commute::rpc::filters
