#include <commute/rpc/filters/error.hpp>

#include <commute/rpc/core/server/metadata.hpp>

#include <commute/rt/locate/transport.hpp>

#include <await/futures/combine/seq/or_else.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class ErrorAttrsEndpoint final : public IEndpoint {
 public:
  ErrorAttrsEndpoint(IEndpointPtr endpoint)
      : continue_(std::move(endpoint)),
        host_(rt::Transport().HostName()) {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto meta = GetMetadata(request.context);

    return continue_->Call(std::move(request)) |
        await::futures::OrElse([meta, host = host_](fallible::Error err) mutable -> fallible::Result<ByteMessage> {
          {
            // Add attributes
            err.AddAttr("host", host);
            err.AddAttr("request.id", meta.id);
            err.AddAttr("request.method", meta.method.FullName());
          }
          // Still fail request
          return fallible::Fail(err);
        });
  }

  const std::string& Name() const override {
    return continue_->Name();
  }

 private:
  IEndpointPtr continue_;
  std::string host_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr AddErrorAttrs(IEndpointPtr endpoint) {
  return std::make_shared<ErrorAttrsEndpoint>(std::move(endpoint));
}

}  // namespace commute::rpc::filters
