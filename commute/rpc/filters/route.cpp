#include <commute/rpc/filters/route.hpp>

#include <commute/rpc/filters/registry.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <fallible/error/error.hpp>
#include <fallible/error/codes.hpp>

#include <timber/log/log.hpp>

#include <map>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class NotFound final : public IEndpoint {
 public:
  BoxedFuture<ByteMessage> Call(Request request) override {
    return futures::Fail<ByteMessage>(
        ServiceNotFound(request.method));
  }

  const std::string& Name() const override {
    static const std::string kName = "_NotFound";
    return kName;
  }

 private:
  static fallible::Error ServiceNotFound(const Method& method) {
    return fallible::errors::NotFound()
        .Domain(rpc::errors::Domain())
        .Reason(fmt::format("Service `{}` not found", method.service))
        .Done();
  }
};

//////////////////////////////////////////////////////////////////////

class Router final : public IRouter {
 public:
  Router(IEndpointPtr or_continue)
      : or_(std::move(or_continue)) {
  }

  void Add(IEndpointPtr endpoint) override {
    registry_.Register(endpoint->Name(), endpoint);
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto endpoint = registry_.TryGet(request.method.service);

    if (endpoint) {
      return endpoint->Call(std::move(request));
    } else {
      return or_->Call(std::move(request));
    }
  }

  const std::string& Name() const override {
    static const std::string kName = "_Router";
    return kName;
  }

 private:

 private:
  IEndpointPtr or_;
  // Routing table
  Registry registry_;
};

//////////////////////////////////////////////////////////////////////

IRouterPtr Route(IEndpointPtr or_continue) {
  return std::make_shared<Router>(std::move(or_continue));
}

IRouterPtr Route() {
  return Route(/*or_continue=*/std::make_shared<NotFound>());
}

IEndpointPtr Intercept(IEndpointPtr intercept, IEndpointPtr or_continue) {
  auto router = Route(or_continue);
  router->Add(intercept);
  return router;
}

}  // namespace commute::rpc::filters
