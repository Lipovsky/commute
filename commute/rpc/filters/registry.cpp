#include <commute/rpc/filters/registry.hpp>

#include <wheels/core/panic.hpp>

namespace commute::rpc::filters {

void Registry::Register(std::string name, IEndpointPtr service) {
  if (entries_.contains(name)) {
    WHEELS_PANIC("Endpoint '" << name << "' has already been registered");
  }

  entries_.emplace(std::move(name), std::move(service));
}

bool Registry::Has(const std::string& name) const {
  return entries_.contains(name);
}

IEndpointPtr Registry::Get(const std::string& name) {
  return entries_.at(name);
}

IEndpointPtr Registry::TryGet(const std::string& name) {
  if (auto it = entries_.find(name); it != entries_.end()) {
    return it->second;
  } else {
    return nullptr;
  }
}

}  // namespace commute::rpc::filters
