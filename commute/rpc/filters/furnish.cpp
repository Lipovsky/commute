#include <commute/rpc/filters/furnish.hpp>

#include <commute/rpc/core/server/invoker.hpp>

#include <commute/rpc/filters/trace.hpp>
#include <commute/rpc/filters/deadline.hpp>
#include <commute/rpc/filters/log.hpp>
#include <commute/rpc/filters/error.hpp>
#include <commute/rpc/filters/route.hpp>

#include <commute/rpc/health/health.hpp>

namespace commute::rpc::filters {

IEndpointPtr Furnish(IEndpointPtr raw) {
  auto traced = Trace(std::move(raw));
  auto with_deadline = DeadLine(std::move(traced));

  auto error_attrs = AddErrorAttrs(std::move(with_deadline));

  auto health = Invoke(MakeHealthService());
  auto with_health = Intercept(health, error_attrs);

  auto log = Log(std::move(with_health));

  return log;
}

}  // namespace commute::rpc::filters
