#pragma once

#include <fallible/error/error.hpp>

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

namespace commute::rpc::jet::wire {

void SetError(proto::commute::rpc::jet::Response& rsp, const fallible::Error& e);
fallible::Error GetError(const proto::commute::rpc::jet::Response& rsp);

}  // namespace commute::rpc::jet::wire
