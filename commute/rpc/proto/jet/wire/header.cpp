#include <commute/rpc/proto/jet/wire/header.hpp>

namespace commute::rpc::jet::wire {

//////////////////////////////////////////////////////////////////////

void SetValue(proto::commute::rpc::jet::header::Value* proto, const headers::Value& value) {
  if (value.IsString()) {
    proto->set_str(value.AsString());
  } else if (value.IsUInt64()) {
    proto->set_uint(value.AsUInt64());
  } else if (value.IsBool()) {
    proto->set_flag(value.AsBool());
  } else {
    std::abort();
  }
}

void SetHeader(proto::commute::rpc::jet::header::Header* proto, const headers::Header& header) {
  proto->set_name(header.name);
  SetValue(proto->mutable_value(), header.value);
}

//////////////////////////////////////////////////////////////////////

headers::Value GetValue(const proto::commute::rpc::jet::header::Value& proto) {
  using headers::Value;

  if (proto.has_str()) {
    return Value(proto.str());
  } else if (proto.has_uint()) {
    return Value(proto.uint());
  } else if (proto.has_flag()) {
    return Value(proto.flag());
  } else {
    std::abort();
  }
}

headers::Header GetHeader(const proto::commute::rpc::jet::header::Header& proto) {
  return {proto.name(), GetValue(proto.value())};
}

}  // namespace commute::rpc::jet::wire
