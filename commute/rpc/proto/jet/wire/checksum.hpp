#pragma once

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

#include <commute/support/checksum.hpp>

namespace commute::rpc::jet::wire {

support::Checksum::Value ComputeChecksum(
    const proto::commute::rpc::jet::Request& request);

support::Checksum::Value ComputeChecksum(
    const proto::commute::rpc::jet::Response& response);

}  // namespace commute::rpc::jet::wire
