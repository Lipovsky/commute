#pragma once

#include <commute/rpc/context/headers.hpp>

#include <commute/rpc/proto/jet/proto/cpp/header.pb.h>

namespace commute::rpc::jet::wire {

void SetHeader(proto::commute::rpc::jet::header::Header* proto, const headers::Header& header);
headers::Header GetHeader(const proto::commute::rpc::jet::header::Header& proto);

}  // namespace commute::rpc::jet::wire
