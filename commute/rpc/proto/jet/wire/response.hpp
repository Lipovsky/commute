#pragma once

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

namespace commute::rpc::jet::wire {

void PrepareResponse(proto::commute::rpc::jet::Response& response,
                     const proto::commute::rpc::jet::Request& request);

void FinalizeResponse(proto::commute::rpc::jet::Response& response);

}  // namespace commute::rpc::jet::wire
