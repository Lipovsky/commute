#pragma once

#include <commute/rpc/core/request/method.hpp>
#include <commute/rpc/core/request/message.hpp>
#include <commute/rpc/core/request/id.hpp>

#include <commute/rpc/core/client/channel.hpp>

#include <commute/transport/transport.hpp>

#include <await/tasks/exe/strand.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/contract.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/run/go.hpp>

#include <snowflake/id/generator.hpp>

#include <carry/delayed.hpp>

#include <fallible/error/error.hpp>

#include <timber/log/logger.hpp>

#include <map>
#include <optional>

namespace commute::rpc::jet {

//////////////////////////////////////////////////////////////////////

// Fair-loss channel

class SocketChannel : public IChannel,
                      public transport::IHandler,
                      public std::enable_shared_from_this<SocketChannel> {
 private:
  struct ActiveRequest {
    Request request;
    RequestId id;
    
    std::optional<await::futures::eager::Promise<fallible::Result<ByteMessage>>> promise;
  };

  using ActiveRequests = std::map<RequestId, ActiveRequest>;

 public:
  SocketChannel(std::string peer);
  ~SocketChannel();

  void Start() {
    // GetTransportSocket();
  }

  // rpc::IChannel

  BoxedFuture<ByteMessage> Call(Request request) override;

  live::Status Status() override {
    return live::Status::Alive;
  }

  void Close() override;

  const std::string& Peer() const override {
    return peer_address_;
  }

  // transport::IHandler

  void HandleMessage(const transport::Message& message,
                     transport::ISocketPtr /*back*/) override {
    await::futures::Submit(strand_, [self = shared_from_this(), message]() {
      self->HandleResponse(message);
    }) | await::futures::Go();
  }

  void HandleDisconnect(const std::string& /*peer*/) override {
    await::futures::Submit(strand_, [self = shared_from_this()]() {
      self->LostPeer();
    }) | await::futures::Go();
  }

 private:
  BoxedFuture<ByteMessage> Send(Request request);
  void HandleResponse(const transport::Message& message);
  void CancelRequest(const RequestId& request_id);
  void SendCancelRequest(const RequestId& request_id, const Method& method);
  void LostPeer();
  void DoClose();

  RequestId GenerateRequestId();

 private:
  transport::ISocketPtr& GetTransportSocket();

  void Fail(ActiveRequest& request, fallible::Error error, carry::Context user);

 private:
  const std::string peer_address_;

  // Runtime
  await::tasks::IExecutor& executor_;
  transport::ITransport& transport_;
  snowflake::id::IGenerator& ids_;

  await::tasks::Strand strand_;
  // State guarded by strand_
  transport::ISocketPtr socket_{nullptr};
  ActiveRequests requests_;

  timber::log::Logger logger_;
};

}  // namespace commute::rpc::jet
