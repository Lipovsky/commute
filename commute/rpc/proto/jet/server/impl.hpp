#pragma once

#include <commute/rpc/core/server/server.hpp>
#include <commute/rpc/core/server/endpoint.hpp>
#include <commute/rpc/core/server/handlers.hpp>

#include <commute/rpc/core/request/message.hpp>
#include <commute/rpc/core/request/id.hpp>
#include <commute/rpc/errors/domain.hpp>

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

#include <commute/transport/transport.hpp>

#include <await/tasks/core/executor.hpp>

#include <timber/log/logger.hpp>

#include <functional>
#include <memory>

namespace commute::rpc::jet {

// Server

//////////////////////////////////////////////////////////////////////

class Server : public IServer,
               public transport::IHandler,
               public std::enable_shared_from_this<Server> {
 public:
  Server(std::string port);

  // rpc::IServer

  void Serve(IEndpointPtr endpoint) override;
  void Shutdown() override;

  std::string Port() const override {
    return server_->Port();
  }

  // transport::IHandler

  void HandleMessage(const transport::Message& message,
                     transport::ISocketPtr client) override;

  void HandleDisconnect(const std::string& client) override;

 private:
  void Register(const std::string& name, IEndpointPtr service);

  fallible::Result<proto::commute::rpc::jet::Request> TryParseRequest(const transport::Message& message);

  void HandleRequest(const proto::commute::rpc::jet::Request& request,
                     const transport::ISocketPtr& client);


  void Respond(const ::proto::commute::rpc::jet::Request& request,
               fallible::Result<ByteMessage> result,
               const transport::ISocketPtr& client);

  void RespondWithError(const ::proto::commute::rpc::jet::Request& request,
                        const transport::ISocketPtr& client,
                        fallible::Error error);

  void SendResponse(::proto::commute::rpc::jet::Response& response,
                    const transport::ISocketPtr& client);

 private:
  const std::string port_;

  // Runtime
  await::tasks::IExecutor& executor_;
  transport::ITransport& transport_;

  transport::IServerPtr server_;

  IHandlerManagerPtr handlers_;
  IEndpointPtr endpoint_;

  timber::log::Logger logger_;
};

}  // namespace commute::rpc::jet
