#include <commute/rpc/errors/retriable.hpp>

#include <fallible/error/codes.hpp>

namespace commute::rpc::errors {

bool IsRetriable(const fallible::Error& error) {
  switch (error.Code()) {
    // Transient errors
    case fallible::ErrorCodes::Disconnected:
    case fallible::ErrorCodes::Unavailable:
    case fallible::ErrorCodes::TimedOut:
    case fallible::ErrorCodes::Aborted:
      return true;
    default:
      return false;
  }
}

}  // namespace commute::rpc::errors
