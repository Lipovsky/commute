#pragma once

#include <fallible/error/error.hpp>

namespace commute::rpc::errors {

bool IsRetriable(const fallible::Error& error);

}  // namespace commute::rpc::errors
