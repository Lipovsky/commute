#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <commute/rpc/balance/load.hpp>

namespace commute::rpc::balance {

struct INode : IChannel {
  virtual LoadValue Load() = 0;
};

using INodePtr = std::shared_ptr<INode>;

}  // namespace commute::rpc::balance
