#include <commute/rpc/balance/channel.hpp>

#include <commute/rt/locate/timers.hpp>

#include <await/fibers/sched/sleep_for.hpp>
#include <await/futures/make/submit.hpp>
#include <await/futures/run/go.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <timber/log/logger.hpp>
#include <timber/log/log.hpp>

using namespace std::chrono_literals;

namespace commute::rpc::balance {

//////////////////////////////////////////////////////////////////////

class BalancerChannel final :
    public IChannel,
    public std::enable_shared_from_this<BalancerChannel> {
 private:
  using Channels = std::map<std::string, IChannelPtr>;
  using AddrSet = std::set<std::string>;

 public:
  BalancerChannel(IResolverPtr resolver,
                  IClientPtr client,
                  ILoadMetricPtr load,
                  IAlgorithmPtr algo)
    : resolver_(resolver),
      client_(client),
      load_(load),
      algo_(algo),
      logger_("Balancer") {
    Build();
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto node = GetDistributor()->Pick(request);
    return node->Call(std::move(request));
  }

  live::Status Status() override {
    return live::Status::Alive;
  }

  const std::string& Peer() const override {
    static const std::string kName = "Balancer";
    return kName;
  }

  void Close() override {
    Stop();
  }

  void Start() {
    await::futures::Spawn([wb = weak_from_this()]() mutable {
      while (true) {
        await::fibers::SleepFor(rt::Timers().Delay(1s));

        if (auto balancer = wb.lock()) {
          balancer->Update();
        } else {
          break;
        }
      }
    }) | await::futures::Go();
  }

 private:
  IDistributorPtr GetDistributor() {
    twist::ed::Locker locker(mutex_);
    return distributor_;
  }


  void Update() {
    TIMBER_LOG_DEBUG("Update");

    auto addrs = resolver_->Resolve();

    if (Diff(addrs)) {
      Rebuild(ToSet(std::move(addrs)));
    }
  }

  bool Diff(const Addrs& next) const {
    if (next.size() != addrs_.size()) {
      return true;
    }

    for (const auto& addr: next) {
      if (!addrs_.contains(addr)) {
        return true;
      }
    }

    return false;
  }

  IDistributorPtr MakeDistributor(const Channels& out) {
    Nodes nodes;

    for (const auto& [_, chan] : out) {
      nodes.push_back(load_->Measure(chan));
    }

    return algo_->Make(std::move(nodes));
  }

  void Rebuild(AddrSet next) {
    TIMBER_LOG_DEBUG("Rebuild balancer: {} replicas", next.size());

    AddrSet curr = addrs_;

    // Remove replicas
    {
      for (const auto& addr : curr) {
        if (!next.contains(addr)) {
          auto it = channels_.find(addr);
          it->second->Close();
          channels_.erase(it);
        }
      }
    }

    // Add replicas
    {
      for (const auto& addr: next) {
        if (!curr.contains(addr)) {
          channels_.insert_or_assign(addr, client_->Dial(addr));
        }
      }
    }

    auto distributor = MakeDistributor(channels_);

    // Replace distributor

    {
      twist::ed::Locker locker(mutex_);
      addrs_ = std::move(next);
      distributor_ = std::move(distributor);
    }
  }

  void Stop() {
    for (const auto& [_, chan] : channels_) {
      chan->Close();
    }
    channels_.clear();
  }

  void Build() {
    Rebuild(ToSet(resolver_->Resolve()));
  }

 private:
  AddrSet ToSet(Addrs addrs) {
    AddrSet set;
    for (auto&& addr : addrs) {
      set.insert(std::move(addr));
    }
    return set;
  }

 private:
  // Strategies
  IResolverPtr resolver_;
  IClientPtr client_;
  ILoadMetricPtr load_;
  IAlgorithmPtr algo_;

  twist::ed::SpinLock mutex_;
  AddrSet addrs_;
  Channels channels_;
  IDistributorPtr distributor_;

  twist::ed::stdlike::atomic<bool> updates_{false};

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Balancer(
    IResolverPtr resolver,
    IClientPtr client,
    ILoadMetricPtr load,
    IAlgorithmPtr algo) {

  auto balancer = std::make_shared<BalancerChannel>(
      std::move(resolver),
      std::move(client),
      std::move(load),
      std::move(algo));

  balancer->Start();

  return std::move(balancer);
}

}  // namespace commute::rpc::balance
