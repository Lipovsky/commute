#include <commute/rpc/balance/algo/random.hpp>

#include <commute/rt/locate/random.hpp>

namespace commute::rpc::balance::algo {

//////////////////////////////////////////////////////////////////////

class P2CDistributor final : public IDistributor {
 public:
  P2CDistributor(Nodes nodes)
      : nodes_(std::move(nodes)) {
  }

  INodePtr Pick(const Request&) override {
    static const size_t kTries = 3;

    for (size_t i = 0; i + 1 < kTries; ++i) {
      INodePtr node = TryPick();
      if (node->IsAlive()) {
        return node;
      }
    }

    return TryPick();
  }

 private:
  INodePtr Choice(const INodePtr& fst, const INodePtr& snd) {
    {
      auto fst_status = fst->Status();
      auto snd_status = snd->Status();

      if (fst_status != snd_status) {
        return (fst_status < snd_status) ? fst : snd;
      }
    }

    auto fst_load = fst->Load();
    auto snd_load = snd->Load();

    return (fst_load < snd_load) ? fst : snd;
  }

  size_t RandomIndex() const {
    return rt::Random().Choice(nodes_.size());
  }

  INodePtr TryPick() {
    size_t fst = RandomIndex();
    size_t snd = RandomIndex();

    if (snd == fst) {
      snd = (snd + 1) % nodes_.size();
    }

    return Choice(nodes_[fst], nodes_[snd]);
  }

 private:
  const Nodes nodes_;
};

//////////////////////////////////////////////////////////////////////

struct P2CAlgorithm : IAlgorithm {
  IDistributorPtr Make(Nodes nodes) override {
    return std::make_shared<P2CDistributor>(std::move(nodes));
  }
};

//////////////////////////////////////////////////////////////////////

IAlgorithmPtr PowerOf2Choices() {
  return std::make_shared<P2CAlgorithm>();
}

}  // namespace commute::rpc::balance::algo
