#include <commute/rpc/balance/algo/rr.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace commute::rpc::balance::algo {

//////////////////////////////////////////////////////////////////////

struct RoundRobinDistributor final : public IDistributor {
 public:
  RoundRobinDistributor(Nodes nodes)
    : nodes_(std::move(nodes)) {
  }

  INodePtr Pick(const Request&) override {
    return nodes_[NextIndex()];
  }

 private:
  size_t NextIndex() {
    return next_index_.fetch_add(1) % nodes_.size();
  }

 private:
  const Nodes nodes_;
  twist::ed::stdlike::atomic<size_t> next_index_{0};
};

//////////////////////////////////////////////////////////////////////

struct RoundRobinAlgorithm : IAlgorithm {
  IDistributorPtr Make(Nodes nodes) override {
    return std::make_shared<RoundRobinDistributor>(std::move(nodes));
  }
};

//////////////////////////////////////////////////////////////////////

IAlgorithmPtr RoundRobin() {
  return std::make_shared<RoundRobinAlgorithm>();
}

}  // namespace commute::rpc::balance::algo
