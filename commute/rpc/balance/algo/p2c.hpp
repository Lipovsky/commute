#pragma once

#include <commute/rpc/balance/algo.hpp>

namespace commute::rpc::balance::algo {

// The Power of Two Random Choices: A Survey of Techniques and Results
// Michael Mitzenmacher et al.
// http://www.eecs.harvard.edu/~michaelm/postscripts/handbook2001.pdf

// The power of two random choices
// https://brooker.co.za/blog/2012/01/17/two-random.html

IAlgorithmPtr PowerOf2Choices();

}  // namespace commute::rpc::balance::algo
