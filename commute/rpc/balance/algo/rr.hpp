#pragma once

#include <commute/rpc/balance/algo.hpp>

namespace commute::rpc::balance::algo {

IAlgorithmPtr RoundRobin();

}  // namespace commute::rpc::balance::algo
