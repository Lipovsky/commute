#pragma once

#include <commute/rpc/balance/metric.hpp>

#include <chrono>

namespace commute::rpc::balance::load {

//////////////////////////////////////////////////////////////////////

using WindowUnits = std::chrono::nanoseconds;

struct EmaParams {
  WindowUnits window = std::chrono::seconds(60);
  double init_value = 1;

  using Params = EmaParams;

  Params& Window(std::chrono::nanoseconds nanos) {
    window = nanos;
    return *this;
  }

  Params& Initial(double value) {
    init_value = value;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

// Exponential moving average for RTT
// NB: Non-deterministic

ILoadMetricPtr EmaRtt(EmaParams params);

}  // namespace commute::rpc::balance::load
