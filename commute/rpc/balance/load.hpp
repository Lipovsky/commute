#pragma once

#include <cstdlib>

namespace commute::rpc::balance {

using LoadValue = uint64_t;

}  // namespace commute::rpc::balance
