#pragma once

#include <commute/rpc/core/client/channel.hpp>
#include <commute/rpc/core/client/client.hpp>

#include <commute/rpc/balance/metric.hpp>
#include <commute/rpc/balance/algo.hpp>

#include <commute/rpc/resolve/resolver.hpp>

namespace commute::rpc::balance {

IChannelPtr Balancer(
    IResolverPtr resolver,
    IClientPtr client,
    ILoadMetricPtr metric,
    IAlgorithmPtr algo);

}  // namespace commute::rpc::balance
