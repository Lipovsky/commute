#include <commute/rpc/channels/failsafe/retry.hpp>

#include <commute/rpc/errors/retriable.hpp>

#include <commute/rpc/context/params.hpp>
#include <commute/rpc/context/headers.hpp>

#include <commute/rt/locate/executor.hpp>
#include <commute/rt/locate/timers.hpp>

#include <carry/delayed.hpp>

#include <timber/log/log.hpp>

#include <fallible/error/codes.hpp>
#include <fallible/result/make.hpp>

#include <await/timers/core/timer_keeper.hpp>

#include <utility>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

// await::futures::lazy::Thunk

class Retrier final :
    public commute::futures::IConsumer<ByteMessage>,
    public await::timers::TimerBase {
 public:
  using ValueType = fallible::Result<ByteMessage>;

 public:
  Retrier(const IChannelPtr& channel,
          Request request,
          retry::IRequestBackoffPtr backoff,
          retry::IBudgetPtr budget)
      : continue_(channel),
        request_(std::move(request)),
        timers_(rt::Timers()),
        attempts_limit_(AttemptsLimit(request_.context)),
        backoff_(std::move(backoff)),
        budget_(std::move(budget)),
        call_(Attempt()),
        logger_("Retry-Channel") {
    static_assert(commute::Future<Retrier, ByteMessage>);
  }

  // Lazy protocol

  void Start(futures::IConsumer<ByteMessage>* consumer) {
    consumer_ = consumer;
    Start();
  }

  // await::futures::IConsumer<Message>

  void Consume(futures::Output<ByteMessage> response) noexcept override {
    HandleAttempt(std::move(response));
  }

  void Cancel(await::futures::Context context) noexcept override {
    consumer_->Cancel(std::move(context));
  }

  await::cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  // await::timers::ITimerHandler

  void Alarm() override {
    Retry();
  }

  // await::timers::ITimer

  std::chrono::milliseconds Delay() const override {
    return delay_;
  }

  bool Periodic() const override {
    return false;
  }

 private:
  BoxedFuture<ByteMessage> Attempt() {
    ++attempt_;
    return continue_->Call(request_);
  }

  void Start() {
    call_.Start(this);
  }

  void Retry() {
    if (CancelRequested()) {
      consumer_->Cancel({});
      return;
    }

    TIMBER_LOG_DEBUG("Retry {}.{} request, attempt {}", continue_->Peer(), request_.method,
                     attempt_);

    call_ = Attempt();
    Start();
  }

  void HandleAttempt(futures::Output<ByteMessage> output) {
    if (CancelRequested()) {
      consumer_->Cancel(output.context);
      return;
    }

    if (output.value.IsOk() || !IsRetriable(output)) {
      if (output.value.IsOk()) {
        budget_->Deposit();
      }
      consumer_->Consume(std::move(output));
    } else {
      ScheduleRetry();
    }
  }

  void ScheduleRetry() {
    if (AttemptsLimitReached()) {
      Stop(fmt::format("Attempts limit reached: {}", attempts_limit_));
      return;
    }

    if (!budget_->TryWithdraw()) {
      Stop("Retry budget exhausted");
      return;
    }

    delay_ = backoff_->Next();
    TIMBER_LOG_DEBUG("Schedule next retry after {}ms", delay_.count());
    timers_.AddTimer(this);
  }

  bool CancelRequested() const {
    return consumer_->CancelToken().CancelRequested();
  }

  bool AttemptsLimitReached() const {
    return (attempts_limit_ > 0) && (attempt_ >= attempts_limit_);
  }

  static size_t AttemptsLimit(const carry::Context& context) {
    if (auto limit = context.TryGet<uint64_t>(params::ContextKey("attempts"))) {
      return *limit;
    } else {
      return 0;
    }
  }

  void HandleCancel(const std::string& reason) {
    TIMBER_LOG_INFO("Request cancelled: {}", reason);
    consumer_->Cancel({});
  }

  bool IsRetriableHeader(const carry::Context& ctx) {
    auto retriable = rpc::headers::TryGet(ctx, "retriable");
    return retriable.has_value() && retriable->AsBool();
  }

  bool IsRetriable(const futures::Output<ByteMessage>& output) {
    return rpc::errors::IsRetriable(output.value.Error()) ||
           IsRetriableHeader(output.context.user);
  }

  void Stop(std::string reason, wheels::SourceLocation from = wheels::Here()) {
    consumer_->Consume(fallible::Fail(
        fallible::errors::ResourceExhausted()
          .Reason(std::move(reason))
          .Location(from)
          .Done()
        ));
  }

 private:
  IChannelPtr continue_;
  Request request_;

  // Runtime
  await::timers::ITimerKeeper& timers_;

  size_t attempts_limit_;

  size_t attempt_ = 0;
  retry::IRequestBackoffPtr backoff_;
  retry::IBudgetPtr budget_;

  BoxedFuture<ByteMessage> call_;

  commute::futures::IConsumer<ByteMessage>* consumer_{nullptr};

  // ITimer
  std::chrono::milliseconds delay_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

class RetriesChannel : public std::enable_shared_from_this<RetriesChannel>,
                       public IChannel {
 public:
  RetriesChannel(IChannelPtr underlying,
                 retry::IBackoffPtr backoff,
                 retry::IBudgetPtr budget)
      : underlying_(std::move(underlying)),
        backoff_(std::move(backoff)),
        budget_(std::move(budget)) {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    return Retrier(underlying_, std::move(request), backoff_->NewRequest(), budget_);
  }

  live::Status Status() override {
    return underlying_->Status();
  }

  const std::string& Peer() const override {
    return underlying_->Peer();
  }

  void Close() override {
    underlying_->Close();
  }

 private:
  IChannelPtr underlying_;

  retry::IBackoffPtr backoff_;
  retry::IBudgetPtr budget_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Retry(IChannelPtr channel,
                  retry::IBackoffPtr backoff,
                  retry::IBudgetPtr budget) {
  return std::make_shared<RetriesChannel>(
      std::move(channel),
      std::move(backoff),
      std::move(budget));
}

}  // namespace commute::rpc::channels
