#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <chrono>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

struct CircuitBreakerParams {
  size_t max_failures;
  std::chrono::milliseconds reset_timeout;
  size_t success_threshold;

  using Params = CircuitBreakerParams;

  CircuitBreakerParams();

  // Build

  Params& MaxFailures(size_t count) {
    max_failures = count;
    return *this;
  }

  Params& SuccessThreshold(size_t count) {
    success_threshold = count;
    return *this;
  }

  Params& ResetTimeout(std::chrono::milliseconds timeout) {
    reset_timeout = timeout;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IChannelPtr CircuitBreaker(IChannelPtr channel,
                           CircuitBreakerParams params);

}  // namespace commute::rpc::channels
