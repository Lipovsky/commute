#include <commute/rpc/channels/failsafe/timeout.hpp>

#include <commute/rpc/context/params.hpp>
#include <commute/rpc/context/headers.hpp>

#include <commute/rpc/errors/domain.hpp>
#include <commute/rpc/retry/jitter/equal.hpp>

#include <commute/rt/locate/clock.hpp>
#include <commute/rt/locate/timers.hpp>

#include <await/futures/make/after.hpp>
#include <await/futures/combine/par/interrupt.hpp>
#include <await/futures/combine/seq/timeout.hpp>

#include <carry/wrap.hpp>

#include <timber/log/logger.hpp>
#include <timber/log/log.hpp>

static const std::string_view kDeadLineHeaderName = "deadline";

namespace commute::rpc {

namespace channels {

//////////////////////////////////////////////////////////////////////

class TimeoutChannel final : public IChannel {
  using Millis = std::chrono::milliseconds;

 public:
  TimeoutChannel(IChannelPtr channel,
                 TimeoutParams params)
      : continue_(std::move(channel)),
        params_(params),
        jitter_(retry::jitter::Equal()),
        logger_("Timeout-Channel") {
    logger_.AddExtra({"peer", continue_->Peer()});
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto maybe_timeout = params_.timeout;

    if (auto timeout = request.context.TryGet<Millis>(params::ContextKey("timeout.ms"))) {
      maybe_timeout = *timeout;
    }

    if (!maybe_timeout) {
      // Forward call
      return continue_->Call(std::move(request));
    }

    auto timeout = Randomize(*maybe_timeout);

    TIMBER_LOG_DEBUG("Set request timeout: {}ms (randomized, user: {}ms)",
                     timeout.count(), maybe_timeout->count());

    request.context = carry::Wrap(request.context)
        .Set(headers::ContextKey(kDeadLineHeaderName),
             PropagateDeadLine(timeout, request.context))
        .Done();

    auto call = continue_->Call(std::move(request));

    auto delay = rt::Timers().Delay(timeout);

    // Structured concurrency
    return std::move(call) | await::futures::WithTimeout(delay);
  }

  live::Status Status() override {
    return continue_->Status();
  }

  const std::string& Peer() const override {
    return continue_->Peer();
  }

  void Close() override {
    continue_->Close();
  }

 private:
  Millis Randomize(Millis timeout) const {
    return jitter_->Add(timeout);
  }

  headers::Value PropagateDeadLine(Millis timeout, const carry::Context& context) {
    uint64_t curr = rt::WallClock().ToDeadline(timeout).TimeSinceEpoch().count();

    if (auto propagated = headers::TryGet(context, kDeadLineHeaderName)) {
      return std::min(curr, propagated->AsUInt64());
    } else {
      return curr;
    }
  }

  static fallible::Error TimedOut(wheels::SourceLocation from = wheels::Here()) {
    return fallible::errors::TimedOut()
        .Domain(rpc::errors::Domain())
        .Reason("Request timed out")
        .Location(from)
        .Done();
  }

 private:
  IChannelPtr continue_;
  const TimeoutParams params_;

  retry::IJitterPtr jitter_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Timeout(IChannelPtr channel,
                    TimeoutParams params) {
  return std::make_shared<TimeoutChannel>(
      std::move(channel),
      params);
}

}  // namespace channels

//////////////////////////////////////////////////////////////////////

namespace headers::keys {

std::string_view DeadLine() {
  return kDeadLineHeaderName;
}

}  // namespace headers::keys

}  // namespace commute::rpc
