#include <commute/rpc/channels/balance/random.hpp>

#include <commute/rpc/balance/channel.hpp>
#include <commute/rpc/balance/algo/random.hpp>
#include <commute/rpc/balance/load/const.hpp>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

IChannelPtr Random(IResolverPtr resolver, IClientPtr client) {
  return balance::Balancer(
      std::move(resolver),
      std::move(client),
      balance::load::Const(0),
      balance::algo::Random());
}

}  // namespace commute::rpc::channels
