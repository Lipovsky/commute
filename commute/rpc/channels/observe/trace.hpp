#pragma once

#include <commute/rpc/core/client/channel.hpp>

namespace commute::rpc::channels {

IChannelPtr Trace(IChannelPtr channel);

}  // namespace commute::rpc::channels
