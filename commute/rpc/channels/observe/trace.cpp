#include <commute/rpc/channels/observe/trace.hpp>

#include <commute/rpc/context/headers.hpp>

#include <commute/rt/locate/tracer.hpp>

#include <await/futures/combine/seq/anyway.hpp>

#include <timber/trace/carry.hpp>

#include <fmt/core.h>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

class TraceChannel final : public IChannel {
 public:
  TraceChannel(IChannelPtr channel)
      : continue_(std::move(channel)),
        tracer_(rt::Tracer()) {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto span = StartSpan(request.method, request.context);

    // Rewrite trace context
    // TODO: Single wrap
    request.context = SetHeaders(timber::trace::Wrap(request.context, span), span);

    return continue_->Call(std::move(request)) |
        await::futures::Anyway([span = std::move(span)]() mutable {
          std::move(span).Finish();  // TODO: Finish or Cancel
        });
  }

  const std::string& Peer() const override {
    return continue_->Peer();
  }

  live::Status Status() override {
    return continue_->Status();
  }

  void Close() override {
    continue_->Close();
  }

 private:
  carry::Context SetHeaders(carry::Context context, timber::trace::Span span) {
    return carry::Wrap(std::move(context))
      .Set<headers::Value>(headers::ContextKey("tracing.trace_id"), span.TraceId())
      .Set<headers::Value>(headers::ContextKey("tracing.span_id"), span.SpanId());
  }

  std::string SpanName(const Method& method) const {
    return fmt::format("Client.{}.{}", method.service, method.name);
  }

  timber::trace::References RefsFrom(const carry::Context& context) {
    if (auto span = context.TryGet<timber::trace::Span>("commute.tracing.span")) {
      // Current span
      return timber::trace::References::ChildOf(span->Context());
      // Just propagated span context
    } else if (auto span_context = context.TryGet<timber::trace::SpanContext>("commute.rpc.context.tracing")) {
      return timber::trace::References::ChildOf(*span_context);
    } else {
      // Source?
      return timber::trace::References::Empty();
    }
  }

  timber::trace::Span StartSpan(const Method& method,
                                const carry::Context& context) {
    return tracer_.StartSpan(SpanName(method), RefsFrom(context));
  }

 private:
  IChannelPtr continue_;
  timber::trace::ITracer& tracer_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Trace(IChannelPtr channel) {
  return std::make_shared<TraceChannel>(std::move(channel));
}

}  // namespace commute::rpc::channels
