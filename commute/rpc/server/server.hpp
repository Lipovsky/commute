#pragma once

#include <commute/rpc/core/server/server.hpp>

namespace commute::rpc {

IServerPtr MakeServer(std::string port);

}  // namespace commute::rpc
