#pragma once

#include <commute/rpc/core/service/service_base.hpp>

#include <commute/rpc/core/server/invoker.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

/*
 * class ExampleService
 *   : public commute::rpc::ServiceBase<ExampleService> {
 *  public:
 *   void Hello(const proto::example::Request& request,
 *              Response<proto::example::Response>& response) override {
 *     response->set_data(request.data());  // <- Your code goes here
 *   }
 *
 *   const std::string& Name() const override {
 *     static const std::string kName = "Example";
 *     return kName;
 *   }
 *
 *  private:
 *   void RegisterMethods() override {
 *     COMMUTE_RPC_REGISTER_METHOD(Hello);
 *   }
 * };
 *
 */

//////////////////////////////////////////////////////////////////////

template <typename TService, typename ... Args>
IEndpointPtr Make(Args&& ... args) {
  auto service = std::make_shared<TService>(std::forward<Args>(args)...);
  return rpc::Invoke(std::move(service));
}

}  // namespace commute::rpc
