#pragma once

#include <commute/rpc/live/status.hpp>

namespace commute::rpc::live {

struct IFailureDetector {
  virtual ~IFailureDetector() = default;

  virtual Status Status() = 0;

  bool IsAlive() {
    return Status() == live::Status::Alive;
  }
};

}  // namespace commute::rpc::live
