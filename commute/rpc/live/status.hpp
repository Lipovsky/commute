#pragma once

namespace commute::rpc::live {

enum class Status {
  Alive = 1,
  Suspicious = 2,
  Closed = 3,
};

}  // namespace commute::rpc::live
