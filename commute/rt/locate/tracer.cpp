#include <commute/rt/locate/tracer.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

timber::trace::ITracer& Tracer() {
  return compass::Locate<timber::trace::ITracer>();
}

}  // namespace commute::rt
