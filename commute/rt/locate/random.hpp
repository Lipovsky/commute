#pragma once

#include <snowflake/random/generator.hpp>

namespace commute::rt {

snowflake::random::IGenerator& Random();

}  // namespace commute::rt
