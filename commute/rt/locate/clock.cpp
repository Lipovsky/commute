#include <commute/rt/locate/clock.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

ticktock::IWallClock& WallClock() {
  return compass::Locate<ticktock::IWallClock>();
}

}  // namespace commute::rt
