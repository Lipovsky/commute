#include <commute/rt/locate/executor.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

await::tasks::IExecutor& Executor() {
  return compass::Locate<await::tasks::IExecutor>();
}

}  // namespace commute::rt
