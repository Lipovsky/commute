#include <commute/rt/locate/transport.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

transport::ITransport& Transport() {
  return compass::Locate<transport::ITransport>();
}

}  // namespace commute::rt
