#pragma once

#include <await/tasks/core/executor.hpp>

namespace commute::rt {

await::tasks::IExecutor& Executor();

}  // namespace commute::rt
