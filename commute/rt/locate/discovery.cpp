#include <commute/rt/locate/discovery.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

discovery::IDiscovery& Discovery() {
  return compass::Locate<discovery::IDiscovery>();
}

}  // namespace commute::rt
