#pragma once

#include <snowflake/id/generator.hpp>

namespace commute::rt {

snowflake::id::IGenerator& Ids();

}  // namespace commute::rt
