#pragma once

#include <timber/trace/tracer.hpp>

namespace commute::rt {

timber::trace::ITracer& Tracer();

}  // namespace commute::rt
