#include <commute/rt/matrix/log.hpp>
#include <commute/rt/matrix/clock.hpp>

#include <await/fibers/sched/self.hpp>

#include <await/tasks/curr/context.hpp>

#include <timber/log/level/format.hpp>
#include <timber/log/print/extra.hpp>

#include <timber/trace/span.hpp>
#include <timber/trace/carry.hpp>
#include <timber/trace/log.hpp>

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <sstream>

namespace commute::matrix {

static std::string ThisFiberName() {
  if (await::fibers::AmIFiber()) {
    return fmt::format("Fiber-{}", await::fibers::self::GetId());
  } else {
    return "/";
  }
}

std::string RefsToString(const timber::trace::References& refs) {
  std::stringstream out;
  out << refs;
  return out.str();
}

timber::log::ExtraFields LogBackend::CurrentSiteContext() {
  timber::log::ExtraFields site;

  auto context = await::tasks::curr::Context();

  if (auto span = context.TryGet<timber::trace::Span>(timber::trace::ContextKey())) {
    site.push_back({"trace-id", span->TraceId()});
    site.push_back({"span-id", span->SpanId()});
    site.push_back({"span-name", span->Name()});

    auto refs = span->References();
    if (!refs.IsEmpty()) {
      site.push_back({"refs", RefsToString(refs)});
    }
  }

  return site;
}

void LogBackend::Log(timber::log::Record record) {
  fmt::print("[T {:<10}] -- {:<8} -- {:<15} -- {:<7} -- {}\n",
             clock_.Now().TimeSinceEpoch().count(),
             record.level,
             record.component,
             ThisFiberName(),
             timber::log::WithExtra{record.extra.logger, record.message, record.extra.site});
}

timber::log::Level LogBackend::GetMinLevelFor(const std::string& component) const {
  if (auto it = levels_.find(component); it != levels_.end()) {
    return it->second;
  }
  return default_level_;
}

LogBackend& LogBackend::SetDefaultLevel(timber::log::Level level) {
  default_level_ = level;
  return *this;
}

LogBackend& LogBackend::SetComponentLevel(
    std::string component,
    timber::log::Level level) {
  levels_.insert_or_assign(component, level);
  return *this;
}

}  // namespace commute::matrix
