#pragma once

#include <fmt/core.h>

#include <commute/rt/matrix/clock.hpp>

#include <commute/transport/transport.hpp>

#include <await/tasks/core/executor.hpp>
#include <await/futures/make/submit.hpp>
#include <await/futures/run/go.hpp>

#include <snowflake/random/generator.hpp>

#include <timber/log/backend.hpp>
#include <timber/log/logger.hpp>

#include <chrono>
#include <queue>
#include <map>
#include <tuple>

namespace commute::matrix {

//////////////////////////////////////////////////////////////////////

using Port = uint16_t;

//////////////////////////////////////////////////////////////////////

struct Packet {
  Port from;
  Port to;
  transport::Message message;
};

//////////////////////////////////////////////////////////////////////

struct ILocalTransport {
  virtual ~ILocalTransport() = default;

  virtual void Send(Packet packet) = 0;
  virtual void Close(Port port) = 0;
};

//////////////////////////////////////////////////////////////////////

class Socket : public transport::ISocket {
 public:
  Socket(Port port, Port peer_port, ILocalTransport* localhost,
         bool reply = false)
      : port_(port),
        peer_port_(peer_port),
        peer_(fmt::format("localhost:{}", peer_port_)),
        localhost_(localhost),
        reply_(reply) {
  }

  const std::string& Peer() const {
    return peer_;
  }

  virtual void Send(const transport::Message& message) {
    localhost_->Send({port_, peer_port_, message});
  }

  virtual void Close() {
    if (!reply_) {
      localhost_->Close(port_);
    }
  }

  virtual bool IsConnected() const {
    return true;
  }

  ~Socket() {
    if (!reply_) {
      localhost_->Close(port_);
    }
  }

 private:
  Port port_;
  Port peer_port_;
  std::string peer_;
  ILocalTransport* localhost_;
  bool reply_;
};

//////////////////////////////////////////////////////////////////////

class Server : public transport::IServer {
 public:
  Server(matrix::Port port, ILocalTransport* localhost)
      : port_(port), localhost_(localhost) {
  }

  std::string Port() const override {
    return std::to_string(port_);
  }

  void Shutdown() override {
    localhost_->Close(port_);
  }

 private:
  matrix::Port port_;
  ILocalTransport* localhost_;
};

//////////////////////////////////////////////////////////////////////

using Millis = std::chrono::milliseconds;

//////////////////////////////////////////////////////////////////////

class Queue {
  using TimePoint = matrix::Clock::TimePoint;

  struct Entry {
    Packet packet;
    TimePoint delivery_time;
    size_t tie_braker;

    bool operator>(const Entry& rhs) const {
      return std::tie(delivery_time, tie_braker) > std::tie(rhs.delivery_time, rhs.tie_braker);
    }
  };

 public:
  Queue(matrix::Clock& clock)
    : clock_(clock) {
  }

  void Push(Packet packet, Millis delay) {
    auto delivery_time = clock_.Now() + delay;
    entries_.push({packet, delivery_time, ++counter_});
  }

  bool IsEmpty() const {
    return entries_.empty();
  }

  bool Ready() const {
    return !IsEmpty() && (NextDeliveryTime() <= clock_.Now());
  }

  Packet PopFront() {
    Entry front = entries_.top();
    assert(clock_.Now() == front.delivery_time);
    entries_.pop();
    return front.packet;
  }

  TimePoint NextDeliveryTime() const {
    assert(!IsEmpty());
    return Front().delivery_time;
  }

 private:
  const Entry& Front() const {
    return entries_.top();
  }

 private:
  matrix::Clock& clock_;
  size_t counter_;

  std::priority_queue<Entry, std::vector<Entry>, std::greater<Entry>> entries_;
};


//////////////////////////////////////////////////////////////////////

struct Latency {
  Millis lo;
  Millis hi;

  Latency()
    : lo(Millis(0)),
      hi(Millis(0)) {
  }

  Latency(Millis l, Millis h)
    : lo(l), hi(h) {
  }
};

//////////////////////////////////////////////////////////////////////

class LocalTransport :
    public transport::ITransport,
    public ILocalTransport {

  using Message = transport::Message;

  struct Endpoint {
    transport::IHandlerPtr handler;
  };

 public:
  LocalTransport(await::tasks::IExecutor& executor,
                 snowflake::random::IGenerator& random,
                 matrix::Clock& clock,
                 timber::log::IBackend& log)
      : packets_(clock),
        executor_(executor),
        random_(random),
        logger_("Local-Transport", log) {
  }

  void SetLatency(Latency latency) {
    latency_ = latency;
  }

  // ITransport

  const std::string& HostName() const override {
    static const std::string kLocalHost = "localhost";
    return kLocalHost;
  }

  transport::IServerPtr Serve(const std::string& address,
                              transport::IHandlerPtr handler) override;

  transport::ISocketPtr ConnectTo(const std::string& address,
                                  transport::IHandlerPtr handler) override;

  // ILocalHost

  void Send(Packet packet) override;

  void Close(Port port) override {
    endpoints_.erase(port);
  }

  // Run

  bool HasPackets() const {
    return !packets_.IsEmpty();
  }

  bool Ready() const {
    return packets_.Ready();
  }

  Clock::TimePoint NextDeliveryTime() const {
    return packets_.NextDeliveryTime();
  }

  size_t Run();

 private:
  Port FindNextFreePort();

  void Disconnect(Port from, Port to);
  void Handle(Packet packet);

  // Invoke handlers in executor

  void HandleMessage(transport::IHandlerPtr handler,
                     transport::ISocketPtr socket, Message message) {
    await::futures::Submit(executor_, [handler, socket, message]() {
      if (auto h = handler.lock()) {
        h->HandleMessage(message, socket);
      }
    }) | await::futures::Go();
  }

  void HandleDisconnect(transport::IHandlerPtr handler, std::string peer) {
    await::futures::Submit(executor_, [handler, peer]() {
      if (auto h = handler.lock()) {
        h->HandleDisconnect(peer);
      }
    }) | await::futures::Go();
  }

 private:
  Millis PacketDelay() {
    if (latency_.hi.count() == 0) {
      return Millis(0);
    }

    auto range = latency_.hi.count() - latency_.lo.count();
    auto count = latency_.lo.count() + random_.Jitter(range);

    return Millis(count);
  }

 private:
  Latency latency_;

  Port next_port_{0};
  Queue packets_;
  std::map<Port, Endpoint> endpoints_;

  await::tasks::IExecutor& executor_;
  snowflake::random::IGenerator& random_;

  timber::log::Logger logger_;
};

}  // namespace commute::matrix
