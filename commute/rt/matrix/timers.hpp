#pragma once

#include <commute/rt/matrix/clock.hpp>

#include <await/tasks/core/executor.hpp>
#include <await/timers/core/timer_keeper.hpp>

#include <wheels/intrusive/forward_list.hpp>
#include <wheels/core/unit.hpp>

#include <queue>

namespace commute::matrix {

class TimerService : public await::timers::ITimerKeeper {
  using TimePoint = Clock::TimePoint;
  using Duration = Clock::Duration;

  struct TimerEntry {
    TimePoint deadline;
    await::timers::TimerBase* handler;

    bool operator<(const TimerEntry& rhs) const {
      return deadline > rhs.deadline;
    }
  };

 public:
  explicit TimerService(Clock& clock, await::tasks::IExecutor&)
    : clock_(clock) {
  }

  bool HasTimers() const {
    return !timers_.empty();
  }

  // Precondition: HasTimers() == true
  TimePoint NextDeadLine() const {
    return timers_.top().deadline;
  }

  // Returns number of completed timers
  size_t CompleteReadyTimers();

  // ITimerKeeper

  void AddTimer(await::timers::TimerBase* timer) override;

  await::timers::Delay Delay(std::chrono::milliseconds millis) override {
    return {millis, *this};
  }

 private:
  using HandlerList = wheels::IntrusiveForwardList<await::timers::TimerBase>;

   HandlerList GrabReadyTimers();

 private:
  Clock& clock_;
  // await::tasks::IExecutor& executor_;

  std::priority_queue<TimerEntry> timers_;
};

}  // namespace commute::matrix
