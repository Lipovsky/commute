#pragma once

#include <commute/discovery/discovery.hpp>

#include <map>

namespace commute::matrix {

class Discovery : public discovery::IDiscovery {
 public:
  using AddrList = std::vector<std::string>;

  // Service name -> list of addresses
  AddrList Lookup(discovery::Path path) {
    if (auto it = services_.find(path.repr); it != services_.end()) {
      return it->second;
    }
    return {};  // Empty list
  }

  void Add(const std::string& name, std::string addr) {
    services_[name].push_back(std::move(addr));
  }

 private:
  std::map<std::string, AddrList> services_;
};

}  // namespace commute::matrix
