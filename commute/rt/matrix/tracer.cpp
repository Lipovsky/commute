#include <commute/rt/matrix/tracer.hpp>

#include <timber/trace/log.hpp>

#include <timber/log/log.hpp>

#include <sstream>

namespace commute::matrix {

//////////////////////////////////////////////////////////////////////

Span::Span(Tracer& tracer,
           std::string name,
           timber::trace::References refs)
  : tracer_(tracer),
    name_(name),
    trace_id_(tracer.GetTraceId(refs)),
    span_id_(tracer.GenerateSpanId()),
    refs_(std::move(refs)),
    start_time_(tracer.Now()) {
}

void Span::Init() {
  start_time_ = tracer_.Now();
}

void Span::Finish() {
  tracer_.Finish(*this);
}

void Span::Cancel() {
  tracer_.Cancel(*this);
}

//////////////////////////////////////////////////////////////////////

timber::trace::Id Tracer::GetTraceId(const timber::trace::References& refs) {
  if (refs.IsEmpty()) {
    // Root span
    return ids_.GenerateId();
  }

  if (refs.IsChild()) {
    return refs.Parent().TraceId();
  }

  // Batch span
  return ids_.GenerateId() + "-batch";
}

timber::trace::Id Tracer::GenerateSpanId() {
  return ids_.GenerateId();
}

static std::string ToString(const timber::trace::References& refs) {
  std::stringstream out;
  out << refs;
  return out.str();
}

timber::trace::Span Tracer::StartSpan(std::string name, timber::trace::References refs) {
  timber::trace::Span span{std::make_shared<matrix::Span>(*this, std::move(name), std::move(refs))};

  TIMBER_LOG_INFO("Start span {}: span-id = {}, trace-id = {}, refs = {}",
                  span.Name(),
                  span.SpanId(),
                  span.TraceId(),
                  ToString(span.References()));

  return span;
}

void Tracer::Finish(const Span& span) {
  TIMBER_LOG_INFO("Finish span {}: span-id = {}, trace = {}, refs = {}",
                  span.Name(),
                  span.SpanId(),
                  span.TraceId(),
                  ToString(span.References()));
}

void Tracer::Cancel(const Span& /*span*/) {
  // Do not use logging here =(
}

}  // namespace commute::matrix
