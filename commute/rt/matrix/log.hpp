#pragma once

#include <commute/rt/matrix/clock.hpp>

#include <timber/log/backend.hpp>

#include <map>

namespace commute::matrix {

class LogBackend : public timber::log::IBackend {
 public:
  explicit LogBackend(Clock& clock) : clock_(clock) {
  }

  timber::log::Level GetMinLevelFor(const std::string& component) const override;

  timber::log::ExtraFields CurrentSiteContext() override;

  void Log(timber::log::Record record) override;

  // Configuration

  LogBackend& SetDefaultLevel(timber::log::Level level);
  LogBackend& SetComponentLevel(std::string component,
                                timber::log::Level level);

 private:
  Clock& clock_;

  timber::log::Level default_level_{timber::log::Level::All};
  std::map<std::string, timber::log::Level> levels_;
};

}  // namespace commute::matrix
