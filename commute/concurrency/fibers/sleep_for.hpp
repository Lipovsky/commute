#pragma once

#include <await/fibers/sched/sleep_for.hpp>

#include <commute/rt/locate/timers.hpp>

namespace commute::fibers {

inline void SleepFor(std::chrono::milliseconds ms) {
  await::fibers::SleepFor(rt::Timers().Delay(ms));
}

}  // namespace commute::fibers