#pragma once

#include <commute/concurrency/futures/types/naked.hpp>

#include <fallible/result/make.hpp>

// Defaults
#include <await/tasks/exe/inline.hpp>
#include <carry/empty.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <wheels/intrusive/forward_list.hpp>

#include <memory>

namespace commute::concurrency {

class AsyncSemaphore {
 public:
  struct Params {
    size_t permits;
    size_t backlog;
  };

 private:
  class Waiter;

  class Impl {
   public:
    Impl(Params params);

    void Acquire(Waiter* waiter);
    void Release();

   private:
    using Mutex = twist::ed::SpinLock;
    using Locker = twist::ed::Locker<twist::ed::SpinLock>;

   private:
    bool HasRoomForWaiter(Locker&) const;
    fallible::Error QueueOverflow(wheels::SourceLocation = wheels::Here());

   private:
    const Params params_;

    Mutex mutex_;
    size_t permits_;
    wheels::IntrusiveForwardList<Waiter> wait_queue_;
  };

  using ImplRef = std::shared_ptr<Impl>;

  // await::futures::lazy::Thunk
  class Waiter :
      public wheels::IntrusiveForwardListNode<Waiter> {
   public:
    using ValueType = fallible::Status;

    using IUnitConsumer = futures::IConsumer<wheels::Unit>;

    Waiter(ImplRef semaphore)
        : semaphore_(std::move(semaphore)) {
      static_assert(commute::Future<Waiter, wheels::Unit>);
    }

    // Lazy protocol

    void Start(IUnitConsumer* consumer) {
      consumer_ = consumer;
      semaphore_->Acquire(/*waiter=*/this);
    }
    
    // Semaphore decision

    void Accept() {
      consumer_->Consume(fallible::Ok());
    }

    void Reject(fallible::Error error) {
      consumer_->Consume(fallible::Fail(std::move(error)));
    }

   private:
    ImplRef semaphore_;
    IUnitConsumer* consumer_ = nullptr;
  };

 public:
  using Future = Waiter;

 public:
  explicit AsyncSemaphore(Params params);

  Future Acquire();
  void Release();

 private:
  ImplRef impl_;
};

using AsyncPermit = AsyncSemaphore::Future;

}  // namespace commute::concurrency
