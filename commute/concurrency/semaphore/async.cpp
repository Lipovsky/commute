#include <commute/concurrency/semaphore/async.hpp>

#include <fallible/error/make.hpp>

#include <fmt/core.h>

namespace commute::concurrency {

//////////////////////////////////////////////////////////////////////

AsyncSemaphore::Impl::Impl(Params params)
    : params_(params), permits_(params.permits) {
}

bool AsyncSemaphore::Impl::HasRoomForWaiter(Locker&) const {
  return wait_queue_.Size() < params_.backlog;
}

fallible::Error AsyncSemaphore::Impl::QueueOverflow(wheels::SourceLocation call_site) {
  return fallible::errors::ResourceExhausted()
      .Reason(fmt::format("Concurrency limit ({}) and queue limit ({}) reached",
                          params_.permits, params_.backlog))
      .Location(call_site)
      .Done();
}

void AsyncSemaphore::Impl::Acquire(Waiter* waiter) {
  Locker locker(mutex_);

  if (permits_ > 0) {
    // Acquire immediately
    --permits_;
    locker.unlock();
    waiter->Accept();
  } else if (HasRoomForWaiter(locker)) {
    // Add to wait queue
    wait_queue_.PushBack(waiter);
  } else {
    // Reject
    locker.unlock();
    waiter->Reject(QueueOverflow());
  }
}

void AsyncSemaphore::Impl::Release() {
  Locker locker(mutex_);

  if (Waiter* next = wait_queue_.PopFront(); next != nullptr) {
    locker.unlock();
    next->Accept();
  } else {
    ++permits_;
  }
}

//////////////////////////////////////////////////////////////////////

AsyncSemaphore::AsyncSemaphore(Params params)
    : impl_(std::make_shared<Impl>(params)) {
}

AsyncSemaphore::Future AsyncSemaphore::Acquire() {
  return Waiter(impl_);
}

void AsyncSemaphore::Release() {
  impl_->Release();
}

}  // namespace commute::concurrency
