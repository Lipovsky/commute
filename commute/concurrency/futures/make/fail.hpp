#pragma once

#include <commute/concurrency/futures/types/naked.hpp>

#include <await/futures/make/value.hpp>

#include <fallible/error/error.hpp>

namespace commute::futures {

template <typename T>
auto Fail(fallible::Error error) {
  fallible::Result<T> result = fallible::Fail(std::move(error));
  return await::futures::Value(std::move(result));
}

}  // namespace commute::futures