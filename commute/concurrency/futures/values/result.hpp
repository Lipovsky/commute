#pragma once

// Customization points

#include <commute/concurrency/futures/values/result/to_unit.hpp>
#include <commute/concurrency/futures/values/result/timeout.hpp>

#include <commute/concurrency/futures/values/result/is_monad.hpp>
#include <commute/concurrency/futures/values/result/map.hpp>
#include <commute/concurrency/futures/values/result/and_then.hpp>
#include <commute/concurrency/futures/values/result/or_else.hpp>

#include <commute/concurrency/futures/values/result/result.hpp>

#include <commute/concurrency/futures/values/result/interrupt.hpp>
#include <commute/concurrency/futures/values/result/all.hpp>
#include <commute/concurrency/futures/values/result/join.hpp>
