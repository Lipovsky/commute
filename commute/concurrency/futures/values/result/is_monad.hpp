#pragma once

#include <await/futures/impl/value/t/is_monad.hpp>

#include <fallible/result/result.hpp>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsMonad<fallible::Result<T>> : std::true_type {};

}  // namespace match

}  // namespace await::futures::value
