#pragma once

#include <await/futures/impl/value/t/result.hpp>

#include <fallible/result/result.hpp>

namespace await::futures::value {

template <typename T>
struct ResultTraits<fallible::Result<T>> {
  static bool IsOk(const fallible::Result<T>& r) {
    return r.IsOk();
  }
};

}  // namespace await::futures::value
