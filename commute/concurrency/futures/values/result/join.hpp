#pragma once

#include <await/futures/impl/value/t/join.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

namespace await::futures::value {

template <typename T>
struct JoinTraits<fallible::Result<T>> {
  using OutputType = fallible::Status;

  static bool IsOk(const fallible::Result<T>& r) {
    return r.IsOk();
  }

  static OutputType Ok() {
    return fallible::Ok();
  }

  static OutputType PropagateError(fallible::Result<T> r) {
    return fallible::PropagateError(r);
  }
};

}  // namespace await::futures::value
