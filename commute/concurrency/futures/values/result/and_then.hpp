#pragma once

#include <await/futures/impl/value/t/and_then.hpp>

#include <commute/concurrency/futures/values/result/is.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// Result<T> -> (T -> Result<U>) -> Result<U>

template <typename T, typename F>
struct AndThen<fallible::Result<T>, F> {
  F f;

  // Mapper output type
  using R = std::invoke_result_t<F, T>;

  // Requirements for mapper output type
  static_assert(IsResult<R>);

  auto operator()(fallible::Result<T> r) -> R {
    if (r.IsOk()) {
      return f(std::move(*r));
    } else {
      return fallible::PropagateError(r);
    }
  };
};

}  // namespace apply

}  // namespace await::futures::value
