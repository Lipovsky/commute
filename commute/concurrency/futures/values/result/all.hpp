#pragma once

#include <await/futures/impl/value/t/all.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

namespace await::futures::value {

template <typename T>
struct AllTraits<fallible::Result<T>> {
  using VectorType = std::vector<T>;
  using OutputType = fallible::Result<VectorType>;

  static bool IsOk(const fallible::Result<T>& r) {
    return r.IsOk();
  }

  static T UnwrapValue(fallible::Result<T> r) {
    return std::move(*r);
  }

  static OutputType WrapValues(VectorType vs) {
    return fallible::Ok(std::move(vs));
  }

  static OutputType PropagateError(fallible::Result<T> r) {
    return fallible::PropagateError(r);
  }
};

}  // namespace await::futures::value
