#pragma once

#include <await/futures/types/boxed.hpp>

#include <commute/concurrency/futures/types/naked.hpp>

#include <commute/concurrency/futures/values/result.hpp>

namespace commute {

template <typename T>
using BoxedFuture = await::futures::BoxedFuture<fallible::Result<T>>;

}  // namespace commute
