#pragma once

#include <await/futures/types/future.hpp>

#include <commute/concurrency/futures/values/result.hpp>

#include <commute/concurrency/futures/consumer.hpp>
#include <commute/concurrency/futures/output.hpp>

namespace commute {

template <typename F, typename T>
concept Future = await::futures::Future<F, fallible::Result<T>>;

}  // namespace commute
