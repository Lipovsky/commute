#pragma once

#include <string>

namespace commute::discovery {

struct Path {
  std::string repr;
};

}  // namespace commute::discovery
