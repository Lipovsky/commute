#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <wheels/test/test_framework.hpp>

using namespace std::chrono_literals;

using namespace commute;

TEST_SUITE(Backoff) {
  SIMPLE_TEST(Fixed) {
    auto fixed = rpc::retry::backoff::Fixed(3s);

    auto backoff = fixed->NewRequest();

    for (size_t i = 0; i < 5; ++i) {
      ASSERT_EQ(backoff->Next(), 3s);
    }
  }

  SIMPLE_TEST(Exponential) {
    auto params = rpc::retry::backoff::ExponentialParams().Init(1s).Max(10s).Factor(2);

    auto exp = rpc::retry::backoff::Exponential(std::move(params));

    {
      auto backoff = exp->NewRequest();

      ASSERT_EQ(backoff->Next(), 1s);
      ASSERT_EQ(backoff->Next(), 2s);
      ASSERT_EQ(backoff->Next(), 4s);
      ASSERT_EQ(backoff->Next(), 8s);

      ASSERT_EQ(backoff->Next(), 10s);
      ASSERT_EQ(backoff->Next(), 10s);
    }

    {
      auto backoff = exp->NewRequest();

      ASSERT_EQ(backoff->Next(), 1s);
      ASSERT_EQ(backoff->Next(), 2s);
      ASSERT_EQ(backoff->Next(), 4s);
    }
  }

  struct RandomGenerator : snowflake::random::IGenerator {
    uint64_t Jitter(uint64_t max) override {
      return max / 2;
    }

    size_t Choice(size_t /*size*/) override {
      return 0;
    }
  };

  SIMPLE_TEST(WithJitter) {
    RandomGenerator random;

    auto exp = rpc::retry::backoff::WithJitter(
        rpc::retry::backoff::Exponential({1s, 10s, 3}),
        rpc::retry::jitter::Full(random));

    auto backoff = exp->NewRequest();

    ASSERT_EQ(backoff->Next(), 500ms);  // 1s
    ASSERT_EQ(backoff->Next(), 1500ms);  // 3s
    ASSERT_EQ(backoff->Next(), 4500ms);  // 9s
    ASSERT_EQ(backoff->Next(), 5s);  // 10s
  }
}
