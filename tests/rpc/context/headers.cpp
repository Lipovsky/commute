#include <wheels/test/test_framework.hpp>

#include <commute/rpc/context/headers.hpp>

#include <carry/new.hpp>

#include <set>

using namespace commute;

TEST_SUITE(Headers) {
  SIMPLE_TEST(Values) {
    {
       auto str = rpc::headers::Value::String("test");

       ASSERT_TRUE(str.IsString());
       ASSERT_FALSE(str.IsBool());
       ASSERT_FALSE(str.IsUInt64());

       ASSERT_EQ(str.AsString(), "test");
    }

    {
      auto flag = rpc::headers::Value::Bool(true);

      ASSERT_TRUE(flag.IsBool());
      ASSERT_FALSE(flag.IsString());
      ASSERT_FALSE(flag.IsUInt64());

      ASSERT_EQ(flag.AsBool(), true);
    }

    {
      auto attempt = rpc::headers::Value::UInt64(7);

      ASSERT_TRUE(attempt.IsUInt64());
      ASSERT_FALSE(attempt.IsBool());
      ASSERT_FALSE(attempt.IsString());

      ASSERT_EQ(attempt.AsUInt64(), 7);
    }
  }

  SIMPLE_TEST(ContextKey) {
    auto key = rpc::headers::ContextKey("test");
    ASSERT_EQ(rpc::headers::NameFrom(key), "test");
  }

  SIMPLE_TEST(ContextGet) {
    auto ctx = carry::New()
      .Set(rpc::headers::ContextKey("key"), rpc::headers::Value::UInt64(42))
      .Done();

    // std::optional<rpc::headers::Value>
    auto value = rpc::headers::TryGet(ctx, "key");

    ASSERT_TRUE(value);
    ASSERT_EQ(value->AsUInt64(), 42);
  }

  SIMPLE_TEST(ContextList) {
    auto ctx = carry::New()
        .Set(rpc::headers::ContextKey("attempt"), rpc::headers::Value::UInt64(7))
        .Set(rpc::headers::ContextKey("id"), rpc::headers::Value::String("xyz"))
        .Set("params.timeout.ms", 1024)
        .Done();

    auto headers = rpc::headers::List(ctx);

    ASSERT_EQ(headers.size(), 2);

    // Collect header names

    std::set<std::string> names;
    for (const auto& h : headers) {
      names.insert(h.name);
    }

    ASSERT_TRUE(names.contains("attempt"));
    ASSERT_TRUE(names.contains("id"));
  }
}
