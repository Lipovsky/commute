#include <wheels/test/test_framework.hpp>

#include <commute/rpc/balance/algo/random.hpp>
#include <commute/rpc/balance/algo/rr.hpp>
#include <commute/rpc/balance/algo/p2c.hpp>

using namespace commute;

TEST_SUITE(BalanceAlgorithms) {
  SIMPLE_TEST(Random) {

  }

  SIMPLE_TEST(RoundRobin) {

  }

  SIMPLE_TEST(P2C) {

  }
}
