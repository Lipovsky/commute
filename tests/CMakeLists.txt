ProjectLog("Tests")

add_subdirectory(rpc/proto)

file(GLOB_RECURSE TEST_SOURCES ./*.cpp)

add_executable(commute_tests ${TEST_SOURCES})

target_link_libraries(commute_tests
        commute
        commute_tests_rpc_protos)
