#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/channels/observe/trace.hpp>

#include <commute/rpc/test/filters/count.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <await/fibers/sched/sleep_for.hpp>
#include <await/await.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/fibers/sync/semaphore.hpp>
#include <await/futures/make/execute.hpp>

#include <ticktock/clocks/wall_clock.hpp>
#include <snowflake/random/generator.hpp>

#include <timber/log/log.hpp>

#include <compass/locate.hpp>

#include "proto/cpp/atomic.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

/////////////////////////////////////////////////////////////////////

// Simulation parameters

static const size_t kConcurrentClients = 512;
static const size_t kTxsPerClient = 3;

//////////////////////////////////////////////////////////////////////

class Atomic final :
    public rpc::ServiceBase<Atomic> {
 public:
  static constexpr std::string_view kName = "Atomic";

  Atomic()
    : random_(compass::Locate<snowflake::random::IGenerator>()) {
  }

  // Transactions

  void Load(const proto::atomic::tx::Load::Request& /*request*/,
           Response<proto::atomic::tx::Load::Response>& response) {
    response->set_value(value_.load());
  }

  void FetchAdd(const proto::atomic::tx::FetchAdd::Request& request,
      Response<proto::atomic::tx::FetchAdd::Response>& response) {

    uint64_t curr = value_.load();

    await::fibers::SleepFor(RandomPause());

    if (value_.compare_exchange_strong(curr, curr + request.delta())) {
      response->set_value(curr);
    } else {
      response.Fail(fallible::errors::Aborted().Done());
    }
  }

 private:
  std::chrono::milliseconds RandomPause() {
    return std::chrono::milliseconds(random_.Jitter(5));
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Load);
    COMMUTE_RPC_REGISTER_METHOD(FetchAdd);
  }

 private:
  twist::ed::stdlike::atomic<uint64_t> value_{0};
  snowflake::random::IGenerator& random_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto atomic = rpc::Make<Atomic>();
  auto counter = rpc::test::filters::Count(atomic);
  auto stack = counter;

  server->Serve(std::move(stack));

  // Work for some time
  await::fibers::SleepFor(100500s);

  server->Shutdown();

  std::cout << "Total requests: " << counter->Count() << std::endl;
}

//////////////////////////////////////////////////////////////////////

class AtomicStub {
 public:
  AtomicStub(rpc::IChannelPtr stack)
    : stack_(std::move(stack)) {
  }

  uint64_t Load() {
    proto::atomic::tx::Load::Request req;

    auto f = rpc::Call("Atomic.Load")
        .Request(req)
        .Via(stack_)
        .Done()
        .As<proto::atomic::tx::Load::Response>();

    auto resp = await::Await(std::move(f)).ExpectValue();

    return resp.value();
  }

  bool FetchAdd(uint64_t delta) {
    proto::atomic::tx::FetchAdd::Request req;

    req.set_delta(delta);

    auto f = rpc::Call("Atomic.FetchAdd")
        .Request(req)
        .Via(stack_)
        .Done()
        .As<proto::atomic::tx::FetchAdd::Response>();

    auto resp = await::Await(std::move(f)).ExpectValue();

    return resp.value();
  }

 private:
  rpc::IChannelPtr stack_;
};

//////////////////////////////////////////////////////////////////////

// Backoff strategy for retries

rpc::retry::IBackoffPtr ExpBackoffWithJitter() {
    return rpc::retry::backoff::WithJitter(
      rpc::retry::backoff::Exponential({30ms, 3s, 2}),
      rpc::retry::jitter::Full());
}

rpc::retry::IBackoffPtr FixedDelayWithJitter() {
  return rpc::retry::backoff::WithJitter(
      rpc::retry::backoff::Fixed(100ms),
      rpc::retry::jitter::Full());
}

rpc::retry::IBackoffPtr FixedDelay() {
  return rpc::retry::backoff::Fixed(30ms);
}

rpc::retry::IBackoffPtr Backoff() {
  //return FixedDelay();
  //return FixedDelayWithJitter();
  return ExpBackoffWithJitter();
}

//////////////////////////////////////////////////////////////////////

// Client

std::chrono::milliseconds ToMillis(std::chrono::nanoseconds ns) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(ns);
}

ticktock::IWallClock& WallClock() {
  return compass::Locate<ticktock::IWallClock>();
}

void Coordinator() {
  timber::log::Logger logger_("Coordinator");

  auto start_time = WallClock().Now();

  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(kServerPort);

    // Add automatic retries
    auto retry = rpc::channels::Retry(
        std::move(socket),
        Backoff());

    // Trace request
    auto trace = rpc::channels::Trace(std::move(retry));

    return trace;
  }();

  AtomicStub atomic(std::move(stack));

  await::fibers::WaitGroup burst;

  for (size_t i = 0; i < kConcurrentClients; ++i) {
    burst.Add(await::futures::Spawn([&]() {
      for (size_t j = 0; j < kTxsPerClient; ++j) {
        atomic.FetchAdd(1);
      }
    }));
  }

  burst.Wait();

  std::cout << "Value = " << atomic.Load()
            << " (Expected = " << kConcurrentClients * kTxsPerClient << ") "
            << std::endl;

  auto elapsed_time = WallClock().Now() - start_time;

  std::cout << "All requests completed in "
            << ToMillis(elapsed_time).count() << "ms" << std::endl;
}

//////////////////////////////////////////////////////////////////////

void Sim() {
  matrix::Runtime sim("Backoff");

  // Configure log backend
  sim.LogBackendImpl()
    .SetDefaultLevel(timber::log::Level::Off)  // Supress logging
    .SetComponentLevel("Retry-Channel", timber::log::Level::Info)
    .SetComponentLevel("Coordinator", timber::log::Level::Info);

  sim.SetLatency({10ms, 11ms});

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Coordinator();
  });

  sim.Run();
}

int main() {
  Sim();
  return 0;
}
